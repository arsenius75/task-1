<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

  <style type="text/css">
    body {
      margin:0px;
      padding:0px;
      height:100%;
      align-self: center;
      align-items: center;
      align-content: center;

      overflow:scroll;
    }

    .page {
      min-height:100%;
      position:relative;
    }

    .header {
      padding:10px;
      width:100%;
      text-align:center;
    }

    .content {
      padding:10px;
      padding-bottom:20px; /* Height of the footer element */
      overflow:hidden;
    }


    .body {
      align-self: center;
      align-items: center;
      /*margin:50px 10px 0px 250px;*/
    }


  </style>

<div class="page">
  <tiles:insertAttribute name="header" />
  <div class="content">
    <tiles:insertAttribute name="menu" />
    <tiles:insertAttribute name="body" />
  </div>
  <tiles:insertAttribute name="footer" />
</div>
