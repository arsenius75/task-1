package com.epam.controllers;

import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.*;
import com.epam.newsmanagment.service.INewsManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;

import static com.epam.util.UtilMethods.findNews;

@Controller
@RequestMapping(value = "/newslist/full")
@SessionAttributes(types = SearchCriteria.class)
public class FullNewsController {


    @ModelAttribute("valueObject")
    public NewsManagementVO getValueObject() {
        NewsManagementVO newsManagementVO = new NewsManagementVO();
        newsManagementVO.setNewsItem(new News());
        newsManagementVO.setTagIdList(new ArrayList<Long>());
        newsManagementVO.setAuthorItem(new Author());

        return newsManagementVO;
    }

    @Autowired
    private INewsManagementService newsManagementService;

    @ModelAttribute("comment")
    public Comment getCommentObject() {
        return new Comment();
    }

    @RequestMapping(value = "/{newsId}")
    public String showFullNews(@PathVariable Long newsId, @ModelAttribute("searchCriteria") SearchCriteria searchCriteria, Model model, BindingResult result) {

        try {
            NewsManagementVO vo =  newsManagementService.getSingleNews(newsId);
            if(!model.containsAttribute("isChanged")) {
                vo.getNewsItem().setRownum(newsManagementService.getRownumByNewsId(searchCriteria, newsId));
                model.addAttribute("newsCount", newsManagementService.countAllNews(searchCriteria));
            }

            model.addAttribute("news", vo);

        } catch (ServiceException e) {
            return "error";
        }

        return "fullNews";
    }

    @RequestMapping(value = "/{newsId}/comment")
    public String postCommentToNews( @ModelAttribute("comment") @Valid  Comment comment, BindingResult result, RedirectAttributes redirectAttributes, Model model,
                                     @PathVariable Long newsId, @ModelAttribute("searchCriteria") SearchCriteria searchCriteria) {

        if (result.hasErrors()) {

            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.comment", result);
            redirectAttributes.addFlashAttribute("comment", comment);
            return "redirect:/newslist/full/" + newsId;
        }

        try {
            newsManagementService.addComment(comment.getCommentText(), newsId);
        } catch (ServiceException e) {
            return "error";
        }
        return "redirect:/newslist/full/" + newsId;
    }

    @RequestMapping(value = "/{newsId}/next")
    public String showNextNews(@PathVariable Long newsId, @ModelAttribute("searchCriteria") SearchCriteria searchCriteria, Model model, BindingResult result) {

        Long rownum = null;
        try {
            rownum = newsManagementService.getRownumByNewsId(searchCriteria, newsId);
            NewsManagementVO newsManagementVO = newsManagementService.getSingleNews(newsId);
            newsManagementVO.getNewsItem().setRownum(rownum);
        } catch (ServiceException e) {
            return "error";
        }


        searchCriteria.setMinCount(rownum+1);
        searchCriteria.setMaxCount(rownum + 1);

        return findNews(newsManagementService, searchCriteria);


//        Long nextNewsId = null;
//        try {
//            nextNewsId = newsManagementService.getNextNewsId(searchCriteria, newsId);
//        } catch (ServiceException e) {
//            return "error";
//        }
//
//        return "redirect:/newslist/full/" + nextNewsId;
    }

    @RequestMapping(value = "/{newsId}/previous")
    public String showPreviousNews(@PathVariable Long newsId, @ModelAttribute("searchCriteria") SearchCriteria searchCriteria, Model model, BindingResult result) {

        Long rownum = null;

        try {
            rownum = newsManagementService.getRownumByNewsId(searchCriteria, newsId);
        } catch (ServiceException e) {
            return "error";
        }
        searchCriteria.setMinCount(rownum-1);
        searchCriteria.setMaxCount(rownum-1);

        return findNews(newsManagementService, searchCriteria);

//        Long previousNewsId = null;
//        try {
//            previousNewsId = newsManagementService.getPreviousNewsId(searchCriteria, newsId);
//        } catch (ServiceException e) {
//            return "error";
//        }
//        return "redirect:/newslist/full/" + previousNewsId;

    }

    @RequestMapping(value = "/{newsId}/comment/{commentId}")
    public String deleteComment(@PathVariable Long newsId, @PathVariable Long commentId) {

        try {
            newsManagementService.deleteComment(commentId);
        } catch (ServiceException e) {
            return "error";
        }
        return "redirect:/newslist/full/" + newsId;
    }

}
