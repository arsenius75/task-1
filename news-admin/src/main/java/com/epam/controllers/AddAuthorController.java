package com.epam.controllers;

import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.Author;
import com.epam.newsmanagment.service.INewsManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "/authors")
public class AddAuthorController {


    @Autowired
    private INewsManagementService newsManagementService;


    @ModelAttribute("author")
    public Author getAuthorObject() {
        return new Author();
    }

    @RequestMapping(value = "/")
    public String showAuthorPage(Model model) {

        try {
            model.addAttribute("authors", newsManagementService.getAllAuthors());
        } catch (ServiceException e) {
            return "error";
        }
        model.addAttribute("editingAuthor", null);

        return "addAuthor";
    }

    @RequestMapping(value = "/add")
    public String addAuthor(@ModelAttribute("author") @Valid Author author, BindingResult result, RedirectAttributes redirectAttributes, Model model) {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.author", result);
            redirectAttributes.addFlashAttribute("author", author);
            return "redirect:/authors/";
        }

        try {
            newsManagementService.addAuthor(author);

        } catch (ServiceException e) {
            return "error";
        }
        return "redirect:/authors/";
    }

    @RequestMapping(value = "/update")
    public String updateAuthor(@ModelAttribute("author") @Valid Author author, BindingResult result, RedirectAttributes redirectAttributes, Model model) {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.author", result);
            redirectAttributes.addFlashAttribute("author", author);
            return "redirect:/authors/edit/"+author.getAuthorId();
        }

        try {
            newsManagementService.updateAuthor(author);
        } catch (ServiceException e) {
            return "error";
        }
        return "redirect:/authors/";
    }

    @RequestMapping(value = "/expire/{authorId}")
    public String expireAuthor(@PathVariable("authorId") Long authorId) {

        try {
            newsManagementService.expireAuthor(authorId);
        } catch (ServiceException e) {
            return "error";
        }
        return "redirect:/authors/";
    }

    @RequestMapping(value = "/edit/{authorId}")
    public String editAuthor(@PathVariable Long authorId, Model model) {

        model.addAttribute("editingAuthor", authorId);

        try {
            model.addAttribute("authors", newsManagementService.getAllAuthors());
        } catch (ServiceException e) {
            return "error";
        }
        model.addAttribute("editingAuthor", authorId);

        return "addAuthor";
    }

    @RequestMapping(value = "/cancel")
    public String cancelAuthor(Model model) {

        model.addAttribute("editingAuthor", null);
        return "redirect:/authors/";
    }
}
