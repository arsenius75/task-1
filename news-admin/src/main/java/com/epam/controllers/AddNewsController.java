package com.epam.controllers;

import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.*;
import com.epam.newsmanagment.service.INewsManagementService;
import com.epam.util.DateValidator;
import com.epam.util.entity.Checkbox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static com.epam.util.UtilMethods.getCurrentTimestamp;

@Controller
@RequestMapping(value = "/news")
public class AddNewsController {


    @Autowired
    private INewsManagementService newsManagementService;

    @Autowired
    private MessageSource messageSource;

    @ModelAttribute("valueObject")
    public NewsManagementVO getValueObject() {
        NewsManagementVO newsManagementVO = new NewsManagementVO();
        newsManagementVO.setNewsItem(new News());
        newsManagementVO.setTagIdList(new ArrayList<Long>());
        newsManagementVO.setAuthorItem(new Author());

        return newsManagementVO;
    }

    @RequestMapping(value = "/")
    public String showAddNewsPage(Model model) {

        try {
            model.addAttribute("authors", newsManagementService.getAllAuthors());
            model.addAttribute("tags", newsManagementService.getAllTags());
        } catch (ServiceException e) {
            return "error";
        }

        model.addAttribute("currentTimestamp", getCurrentTimestamp());

        return "addNews";
    }

    @RequestMapping(value = "/add")
    public String addNews(@ModelAttribute("valueObject") @Valid NewsManagementVO newsManagementVO, BindingResult result, RedirectAttributes redirectAttributes, Model model, Locale locale) {

        String datePattern = messageSource.getMessage("label.date_pattern", null, locale);
        Date date = DateValidator.validateDate(newsManagementVO.getNewsItem().getStringDate(), datePattern);
        if (result.hasErrors() || null == date) {

            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.valueObject", result);
            redirectAttributes.addFlashAttribute("valueObject", newsManagementVO);
            if(null == date){
                redirectAttributes.addFlashAttribute("isNotValidDate", true);
            }
            return "redirect:/news/";
        }

        newsManagementVO.getNewsItem().setCreationDate(new Timestamp(date.getTime()));

        Long newsId = null;
        try {
            newsId = newsManagementService.addNews(newsManagementVO);
            NewsManagementVO vo =  newsManagementService.getSingleNews(newsId);

        } catch (ServiceException e) {
            return "error";
        }
        redirectAttributes.addFlashAttribute("isChanged", true);
        return "redirect:/newslist/full/"+newsId;
    }

    @RequestMapping(value = "/{newsId}/edit")
    public String editNews( @ModelAttribute("valueObject") @Valid NewsManagementVO newsManagementVO, BindingResult result,
                            RedirectAttributes redirectAttributes, Model model, @PathVariable Long newsId) {

        try {
            if(null == newsManagementVO.getNewsItem().getFullText()) {
                NewsManagementVO valueObject = newsManagementService.getSingleNews(newsId);
                List<Long> tagIdList = valueObject.getTagList().stream().map(Tag::getTagId).collect(Collectors.toList());
                valueObject.setTagIdList(tagIdList);
                model.addAttribute("valueObject", valueObject);

            }
            model.addAttribute("authors", newsManagementService.getAllAuthors());
            model.addAttribute("tags", newsManagementService.getAllTags());
        } catch (ServiceException e) {
            return "error";
        }

        model.addAttribute("currentTimestamp", getCurrentTimestamp());

        return "editNews";
    }

    @RequestMapping(value = "/update")
    public String updateNews(@ModelAttribute("valueObject") @Valid NewsManagementVO newsManagementVO, BindingResult result, RedirectAttributes redirectAttributes, Model model, Locale locale) {

        String datePattern = messageSource.getMessage("label.date_pattern", null, locale);
        Date date = DateValidator.validateDate(newsManagementVO.getNewsItem().getStringDate(), datePattern);
        if (result.hasErrors() || null == date) {

            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.valueObject", result);
            redirectAttributes.addFlashAttribute("valueObject", newsManagementVO);
            if(null == date){
                redirectAttributes.addFlashAttribute("isNotValidDate", true);
            }

            return "redirect:/news/"+ newsManagementVO.getNewsItem().getNewsId() +"/edit";
        }

        newsManagementVO.getNewsItem().setCreationDate(new Timestamp(date.getTime()));

        try {
            newsManagementService.editNews(newsManagementVO);

        } catch (ServiceException e) {
            return "error";
        }
        redirectAttributes.addFlashAttribute("isChanged", true);
        return "redirect:/newslist/full/"+newsManagementVO.getNewsItem().getNewsId();
    }

    @RequestMapping(value = "/delete")
    public String deleteNews(@ModelAttribute("checkbox") @Valid Checkbox checkbox, BindingResult result) {

        if (result.hasErrors()) {
            return "redirect:/newslist/search/page/1";
        }

        try {
            newsManagementService.deleteNewsList(checkbox.getNewsIDs());

        } catch (ServiceException e) {
            return "error";
        }
        return "redirect:/newslist/search/page/1";
    }
}
