package com.epam.controllers;

import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.Tag;
import com.epam.newsmanagment.service.INewsManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "/tags")
public class AddTagController {


    @Autowired
    private INewsManagementService newsManagementService;


    @ModelAttribute("tag")
    public Tag getTagObject() {
        return new Tag();
    }

    @RequestMapping(value = "/")
    public String showTagPage(Model model) {

        try {
            model.addAttribute("tags", newsManagementService.getAllTags());
        } catch (ServiceException e) {
            return "error";
        }

        model.addAttribute("editingTag", null);


        return "addTag";
    }

    @RequestMapping(value = "/add")
    public String addTag(@ModelAttribute("tag") @Valid Tag tag, BindingResult result, RedirectAttributes redirectAttributes, Model model) {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.tag", result);
            redirectAttributes.addFlashAttribute("tag", tag);
            return "redirect:/tags/";
        }

        try {
            newsManagementService.addTag(tag);

        } catch (ServiceException e) {
            return "error";
        }
        return "redirect:/tags/";
    }

    @RequestMapping(value = "/update")
    public String updateTag(@ModelAttribute("tag") @Valid Tag tag, BindingResult result, RedirectAttributes redirectAttributes, Model model) {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.tag", result);
            redirectAttributes.addFlashAttribute("tag", tag);
            return "redirect:/tags/edit/"+tag.getTagId();
        }

        try {
            newsManagementService.updateTag(tag);
        } catch (ServiceException e) {
            return "error";
        }
        return "redirect:/tags/";
    }

    @RequestMapping(value = "/delete/{tagId}")
    public String deleteTag(@PathVariable Long tagId) {

        try {
            newsManagementService.deleteTag(tagId);
        } catch (ServiceException e) {
            return "error";
        }
        return "redirect:/tags/";
    }


    @RequestMapping(value = "/edit/{tagId}")
    public String editTag(@PathVariable Long tagId, Model model) {

        model.addAttribute("editingTag", tagId);
        try {
            model.addAttribute("tags", newsManagementService.getAllTags());
        } catch (ServiceException e) {
            return "error";
        }
        return "addTag";

    }

    @RequestMapping(value = "/cancel")
    public String cancelTag(Model model) {
        return "redirect:/tags/";
    }
}
