package com.epam.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateValidator { //TODO rename
    public static Date validateDate(String stringDate, String datePattern){
        DateFormat df = new SimpleDateFormat(datePattern);
        df.setLenient(false);
        try {
            return  df.parse(stringDate);
        } catch (ParseException e) {
            return null;
        }
    }
}
