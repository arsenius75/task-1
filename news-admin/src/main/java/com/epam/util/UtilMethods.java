package com.epam.util;

import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.NewsManagementVO;
import com.epam.newsmanagment.model.SearchCriteria;
import com.epam.newsmanagment.service.INewsManagementService;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

public class UtilMethods {
    public static Timestamp getCurrentTimestamp(){
        Calendar calendar = Calendar.getInstance();
        java.util.Date now = calendar.getTime();
        return new java.sql.Timestamp(now.getTime());
    }

    public static Long getLongPageCount(Long newsCount, int newsCountOnPage){
        Double doubleNewsCount = Double.valueOf(newsCount);
        Double pageCount = doubleNewsCount / newsCountOnPage;
        Long longPageCount = pageCount.longValue();

        if (pageCount > longPageCount) {
            longPageCount++;
        }
        return longPageCount;
    }

    public static String findNews(INewsManagementService newsManagementService, SearchCriteria searchCriteria){
        List<NewsManagementVO> newsList = null;
        try {
            newsList = newsManagementService.searchNews(searchCriteria);
        } catch (ServiceException e) {
            return "error";
        }

        if (!newsList.isEmpty()) {
            return "redirect:/newslist/full/" + newsList.get(0).getNewsItem().getNewsId();
        }
        return "error";
    }
}
