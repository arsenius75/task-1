package com.epam.util.entity;

import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;


public class Checkbox {

    @NotEmpty
    private List<Long> newsIDs;

    public List<Long> getNewsIDs() {
        return newsIDs;
    }

    public void setNewsIDs(List<Long> newsIDs) {
        this.newsIDs = newsIDs;
    }
}
