<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
    .error {
        color: #ff0000;
        font-style: italic;
        font-weight: bold;
    }
</style>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">


<div>
  <c:if test="${authors.size()==0}">
    <div align="center">
      <spring:message code="label.nothing_found" />
    </div>
  </c:if>

    <div class="table-responsive">
        <table class="table table-striped"  style="width:30%" align="center">
            <tbody>
  <c:forEach items="${authors}" var="author">
  <tr>


        <spring:url value="/authors/edit/${author.authorId}" var="editAuthor" htmlEscape="true"/>

        <c:if test="${null==editingAuthor || !(author.authorId  eq editingAuthor)}">
                <td>
                        <input value="<c:out value="${author.authorName}"/>" size="38" readonly="readonly" id="inputAuthorName" />
                </td>
                <sf:form action="${editAuthor}">
                    <td>
                        <button type="submit" class="btn btn-lg btn-primary">
                            <spring:message code="label.news.edit" />
                        </button>
                    </td>
            </sf:form>
        </c:if>

        <c:if test="${author.authorId eq editingAuthor}">

            <spring:url value="/authors/update/" var="updateAuthor" htmlEscape="true"/>

                    <sf:form action="${updateAuthor}" modelAttribute="author">

                            <td><spring:message code="label.author" />:</td>
                            <td><sf:input path="authorName" value="${author.authorName}" /></td>
                            <td><sf:input type="hidden" path="authorId" value="${author.authorId}" /></td>
                            <td>
                                <button type="submit" class="btn btn-lg btn-primary">
                                    <spring:message code="label.update" />
                                </button>
                            </td>
                        <td><sf:errors path="authorName" cssClass="error" /></td>
                    </sf:form>

                    <spring:url value="/authors/cancel/" var="cancelAuthor" htmlEscape="true"/>
        <td>
                    <sf:form action="${cancelAuthor}">
                        <button type="submit" class="btn btn-lg btn-primary">
                            <spring:message code="label.cancel" />
                        </button>
                    </sf:form>
        </td>
                        <spring:url value="/authors/expire/" var="expireAuthor" htmlEscape="true"/>
        <td>
                        <sf:form action="${expireAuthor}${author.authorId}">

                            <button type="submit" class="btn btn-lg btn-primary">
                                <spring:message code="label.expire" />
                            </button>

                    </sf:form>
        </td>
        <td>
                    <c:if test="${author.expired!=null}">
                        <spring:message code="label.expired" />
                    </c:if>
        </td>

  </tr>
        </c:if>
    </div>
  </c:forEach>
              </tbody>
          </table>
      </div>
    <spring:url value="/authors/add/" var="addAuthor" htmlEscape="true"/>

      <div align="center">
    <sf:form action="${addAuthor}" modelAttribute="author">
    <tr>
      <td><spring:message code="label.author" />:</td>
        <td><sf:input path="authorName" /></td>
        <td><sf:input type="hidden" path="authorId" value="1"/></td>
      <td>
        <button type="submit" class="btn btn-lg btn-primary">
          <spring:message code="label.save" />
        </button>
      </td>
        <c:if test="${null==editingAuthor || !(author.authorId  eq editingAuthor)}">
            <td><sf:errors path="authorName" cssClass="error" /></td>
        </c:if>


    </tr>
  </sf:form>
          </div>
    <%--<td><sf:errors path="authorName" cssClass="error" /></td>--%>

</div>

