<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:url var="loginAction" value="/login" />

<br/>
<br/>
<br/>
<br/>
<br/>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<div style="width: 40%; margin: 0 auto; text-align: left;">

    <sf:form class="form-signin">
        <h2 class="form-signin-heading"><spring:message code="label.please_sign_in"/></h2>

        <input type="text" name="username" class="form-control" placeholder="Email address" autofocus>

        <input type="password" name="password" class="form-control" placeholder="Password" required>

        <button class="btn btn-lg btn-primary btn-block" type="submit"><spring:message code="label.login"/></button>
    </sf:form>

</div>
    </div>