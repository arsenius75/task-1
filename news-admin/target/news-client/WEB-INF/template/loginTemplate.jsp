<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<style type="text/css">
  body {
    margin:0px;
    padding:0px;
    overflow:scroll;
  }

</style>

<div >
  <tiles:insertAttribute name="header" />
  <div >
    <tiles:insertAttribute name="body" />
  </div>
  <tiles:insertAttribute name="footer" />
</div>
