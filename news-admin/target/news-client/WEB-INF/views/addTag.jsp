<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
  .error {
    color: #ff0000;
    font-style: italic;
    font-weight: bold;
  }
</style>


<div>
  <c:if test="${tags.size()==0}">
    <div align="center">
      <spring:message code="label.nothing_found" />
    </div>
  </c:if>



  <div class="table-responsive">
    <table class="table table-striped"  style="width:30%" align="center">
      <tbody>
  <c:forEach items="${tags}" var="tag">
    <tr>

      <spring:url value="/tags/edit/${tag.tagId}" var="editTag" htmlEscape="true"/>

      <c:if test="${null==editingTag || !(tag.tagId  eq editingTag)}">

          <td><input value="<c:out value="${tag.tagName}"/>" size="28" readonly="readonly"/></td>
          <td>
          <sf:form action="${editTag}">
                <button type="submit" class="btn btn-lg btn-primary">
                  <spring:message code="label.news.edit" />
                </button>
          </sf:form>
          </td>

      </c:if>

      <c:if test="${tag.tagId eq editingTag}">
              <spring:url value="/tags/update/" var="updateTag" htmlEscape="true"/>

              <sf:form action="${updateTag}" modelAttribute="tag">

                  <td><sf:input path="tagName" value="${tag.tagName}" /></td>
                  <sf:input type="hidden" path="tagId" value="${tag.tagId}" />
                  <td>
                    <button type="submit" class="btn btn-lg btn-primary">
                      <spring:message code="label.update" />
                    </button>
                  </td>
                  <td><sf:errors path="tagName" cssClass="error" /></td>

              </sf:form>

            <spring:url value="/tags/delete/${tag.tagId}" var="deleteTag" htmlEscape="true"/>
                <sf:form action="${deleteTag}">
                    <td>
                      <button type="submit" class="btn btn-lg btn-primary">
                        <spring:message code="label.delete" />
                      </button>

                    </td>
                </sf:form>


          <spring:url value="/tags/cancel/" var="cancelTag" htmlEscape="true"/>

            <sf:form action="${cancelTag}">
                <td>
              <button type="submit" class="btn btn-lg btn-primary">
                <spring:message code="label.cancel" />
              </button>

                </td>
            </sf:form>



      </c:if>
    </tr>
  </c:forEach>
      </tbody>
  </table>
</div>


  <div align="center">
  <spring:url value="/tags/add/" var="addTag" htmlEscape="true"/>
  <sf:form action="${addTag}" modelAttribute="tag">
    <tr>
      <td><spring:message code="label.tag" />:</td>
      <td><sf:input path="tagName" /></td>
      <td>
        <button type="submit" class="btn btn-lg btn-primary">
          <spring:message code="label.save" />
        </button>
      </td>
        <c:if test="${null==editingTag || !(tag.tagId  eq editingTag)}">

        <td><sf:errors path="tagName" cssClass="error" /></td>
       </c:if>

    </tr>
  </sf:form>
  </div>


</div>

