<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<style>
  .error {
    color: #ff0000;
    font-style: italic;
    font-weight: bold;
  }
</style>

<div align="center" id="filter">
  <spring:url value="/news/update/" var="updateNews" htmlEscape="true"/>

  <sf:form method="post" action="${updateNews}" modelAttribute="valueObject">

    <sf:input type="hidden" name="id" path="newsItem.newsId" value="${valueObject.newsItem.newsId}"/>
    <sf:input type="hidden" path="newsItem.rownum" />

    <br />
    <sf:select path="authorItem.authorId" id="select1">
      <spring:message code="label.any_author" var="any_author" />
      <sf:option value=""><c:out value="${any_author}"/></sf:option>
      <c:forEach var="author" items="${authors}">
        <sf:option value="${author.authorId}">
          <c:out value="${author.authorName}"/>
        </sf:option>
      </c:forEach>
    </sf:select>


                    <span id="tags" style="text-align: left">
                            <sf:select multiple="true" path="tagIdList" id="select2">
                              <spring:message code="label.any_tag" var="any_tag" />
                              <c:forEach var="tag" items="${tags}">
                                <sf:option value="${tag.tagId}">
                                  <c:out value="${tag.tagName}"/>
                                </sf:option>
                              </c:forEach>
                            </sf:select>

                             <spring:message code="label.any_tag" var="any_tag" />
                        <spring:message code="label.any_author" var="any_author" />
                            <script>
                              $("#select2").SumoSelect({
                                placeholder: '${any_tag}'
                              });
                              $("#select1").SumoSelect({
                                placeholder: '${any_author}'
                              });
                            </script>
		            </span>

    <br/>
    <sf:errors path="tagIdList" cssClass="error" />
    <br/>
    <sf:errors path="authorItem.authorId" cssClass="error" />


    <br/>
    <label><spring:message code="label.title" />:</label>
    <br/>
    <sf:input type="text" name="title" path="newsItem.title" size="50"/>
    <br/>
    <sf:errors path="newsItem.title" cssClass="error" />


    <br />
    <label><spring:message code="label.date" />:</label>
    <br/>
    <spring:message code="label.date_pattern" var="pattern"/>

    <fmt:formatDate value="${valueObject.newsItem.creationDate}" var="dateString" pattern="${pattern}" />

    <sf:input type="text" name="date" value="${dateString}" path="newsItem.stringDate"/>
    <br/>
    <c:if test="${ isNotValidDate eq true}">
      <spring:message code="label.date_not_valid" />
    </c:if>
    <br />

    <label><spring:message code="label.brief" />:</label>
    <br/>
    <sf:input type="text" name="brief" path="newsItem.shortText" size="50"/>
    <br/>
    <sf:errors path="newsItem.shortText" cssClass="error" />

    <br />
    <label><spring:message code="label.content" />:</label>
    <br/>
    <sf:textarea type="text" name="content" path="newsItem.fullText" cols="50" rows="15"/>
    <br/>
    <sf:errors path="newsItem.fullText" cssClass="error" />
    <br/>
    <button type="submit" class="btn btn-xs btn-primary">
      <spring:message code="label.save" />
    </button>
    <br />
  </sf:form>

</div>

<br />
<br />