package com.epam.newsmanagment.model;

import java.util.List;

public class SearchCriteria {

	private Long authorId;
	private List<Long> tagIDs;
	private Long minCount;
	private Long maxCount;

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public List<Long> getTagIDs() {
		return tagIDs;
	}

	public void setTagIDs(List<Long> tagIDs) {
		this.tagIDs = tagIDs;
	}

	public Long getMinCount() {
		return minCount;
	}

	public void setMinCount(Long minCount) {
		this.minCount = minCount;
	}

	public Long getMaxCount() {
		return maxCount;
	}

	public void setMaxCount(Long maxCount) {
		this.maxCount = maxCount;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SearchCriteria that = (SearchCriteria) o;

		if (authorId != null ? !authorId.equals(that.authorId) : that.authorId != null) return false;
		return !(tagIDs != null ? !tagIDs.equals(that.tagIDs) : that.tagIDs != null);

	}

	@Override
	public int hashCode() {
		int result = authorId != null ? authorId.hashCode() : 0;
		result = 31 * result + (tagIDs != null ? tagIDs.hashCode() : 0);
		return result;
	}
}
