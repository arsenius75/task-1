package com.epam.newsmanagment.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import java.util.List;

//@Entity
//@Table(name = "NEWS")
public class NewsManagementVO{

//	@Id
//	@Column(name = "NEWS_ID")
	private Long newsId;

	@Valid
//	@OneToOne
//	@JoinColumn(name = "NEWS_ID")
	private News newsItem;

	@Valid
//	@ManyToOne
//	@JoinTable(name = "NEWS_AUTHORS", joinColumns = {@JoinColumn(name = "NEWS_ID")}, inverseJoinColumns = {@JoinColumn(name = "AUTHOR_ID")})
	private Author authorItem;

//	@OneToMany(mappedBy="news1")
	private List<Comment> commentList;

//	@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE,
//			CascadeType.PERSIST, CascadeType.REFRESH}, mappedBy = "newsList")
	private List<Tag> tagList;

	@NotEmpty
//	@Transient
	private List<Long> tagIdList;

//	@Formula("(SELECT COUNT(*) FROM COMMENTS c where c.NEWS_ID = NEWS_ID)")
//	private Long commentsCount;

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public News getNewsItem() {
		return newsItem;
	}

	public void setNewsItem(News newsItem) {
		this.newsItem = newsItem;
	}

	public Author getAuthorItem() {
		return authorItem;
	}

	public void setAuthorItem(Author authorItem) {
		this.authorItem = authorItem;
	}

	public List<Comment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<Comment> commentList) {
		this.commentList = commentList;
	}

	public List<Tag> getTagList() {
		return tagList;
	}

	public void setTagList(List<Tag> tagList) {
		this.tagList = tagList;
	}


	public List<Long> getTagIdList() {
		return tagIdList;
	}

	public void setTagIdList(List<Long> tagIdList) {
		this.tagIdList = tagIdList;
	}


}
