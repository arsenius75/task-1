package com.epam.newsmanagment.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

@Entity
@Table(name = "COMMENTS")
public class Comment {

	@Id
	@SequenceGenerator(name="comment_seq", initialValue=20, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="comment_seq")
	@Column(name = "COMMENT_ID")
	private Long commentId;

	@Column(name = "NEWS_ID")
	private Long newsId;

    @Size(min=1, max=100)
	@Column(name = "COMMENT_TEXT", length = 100)
    private String commentText;


	@Column(name = "CREATION_DATE")
	private Timestamp creationDate;

	@ManyToOne
	@JoinColumn(name="NEWS_ID", nullable=false, insertable = false, updatable = false)
	private News news1;

	public Comment(Long commentId, Long newsId, String commentText, Timestamp creationDate) {
		super();
		this.commentId = commentId;
		this.newsId = newsId;
		this.commentText = commentText;
		this.creationDate = creationDate;
	}

	public Comment() {

	}

	public Long getCommentId() {
		return commentId;
	}
	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}
	public Long getNewsId() {
		return newsId;
	}
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Comment comment = (Comment) o;

		if (commentId != null ? !commentId.equals(comment.commentId) : comment.commentId != null) return false;
		if (newsId != null ? !newsId.equals(comment.newsId) : comment.newsId != null) return false;
		if (commentText != null ? !commentText.equals(comment.commentText) : comment.commentText != null) return false;
		return !(creationDate != null ? !creationDate.equals(comment.creationDate) : comment.creationDate != null);

	}

	@Override
	public int hashCode() {
		int result = commentId != null ? commentId.hashCode() : 0;
		result = 31 * result + (newsId != null ? newsId.hashCode() : 0);
		result = 31 * result + (commentText != null ? commentText.hashCode() : 0);
		result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
		return result;
	}
}
