package com.epam.newsmanagment.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "NEWS")
public class News {

	@Id
	@SequenceGenerator(name = "news_seq", initialValue = 20, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "news_seq")
	@Column(name = "NEWS_ID")
	private Long newsId;

	@Size(min = 1, max = 30)
	@Column(name = "TITLE", length = 30)
	private String title;

	@Size(min = 1, max = 100)
	@Column(name = "SHORT_TEXT", length = 100)
	private String shortText;

	@Size(min = 1, max = 2000)
	@Column(name = "FULL_TEXT", length = 2000)
	private String fullText;


	@Column(name = "CREATION_DATE")
	private Timestamp creationDate;

	@Column(name = "MODIFICATION_DATE")
	private Date modificationDate;

	@Transient
	private Long rownum;
	@Transient
	private String stringDate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinTable(name = "NEWS_AUTHORS", joinColumns = {@JoinColumn(name = "NEWS_ID")}, inverseJoinColumns = {@JoinColumn(name = "AUTHOR_ID")})
	private Author authorItem;

	@OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE,
			CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.REMOVE}, mappedBy="news1")
	@Fetch(value = FetchMode.SUBSELECT)
	private Set<Comment> commentList;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "NEWS_TAGS", joinColumns = { @JoinColumn(name = "NEWS_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "TAG_ID") })
	@Fetch(value = FetchMode.SUBSELECT)
	private Set<Tag> tagList;

	@Formula("(SELECT COUNT(*) FROM COMMENTS c where c.NEWS_ID = NEWS_ID)")
	private Long commentsCount;


	public News(Long newsId, String title, String shortText, String fullText, Timestamp creationDate, Date modificationDate) {
		super();
		this.newsId = newsId;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	public News(Long newsId, String title, String shortText, String fullText, Timestamp creationDate, Date modificationDate, Long rownum) {
		super();
		this.newsId = newsId;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
		this.rownum = rownum;
	}

	public News() {

	}

	public News(Long l, String t, String d, String d1) {
	}

	public Author getAuthorItem() {
		return authorItem;
	}

	public void setAuthorItem(Author authorItem) {
		this.authorItem = authorItem;
	}

	public Set<Comment> getCommentList() {
		return commentList;
	}

	public void setCommentList(Set<Comment> commentList) {
		this.commentList = commentList;
	}

	public Set<Tag> getTagList() {
		return tagList;
	}

	public void setTagList(Set<Tag> tagList) {
		this.tagList = tagList;
	}

	public Long getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(Long commentsCount) {
		this.commentsCount = commentsCount;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Long getRownum() {
		return rownum;
	}

	public void setRownum(Long rownum) {
		this.rownum = rownum;
	}

	public String getStringDate() {
		return stringDate;
	}

	public void setStringDate(String stringDate) {
		this.stringDate = stringDate;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		News news = (News) o;

		if (newsId != null ? !newsId.equals(news.newsId) : news.newsId != null) return false;
		if (title != null ? !title.equals(news.title) : news.title != null) return false;
		if (shortText != null ? !shortText.equals(news.shortText) : news.shortText != null) return false;
		if (fullText != null ? !fullText.equals(news.fullText) : news.fullText != null) return false;
		if (creationDate != null ? !creationDate.equals(news.creationDate) : news.creationDate != null) return false;
		if (modificationDate != null ? !modificationDate.equals(news.modificationDate) : news.modificationDate != null)
			return false;
		if (rownum != null ? !rownum.equals(news.rownum) : news.rownum != null) return false;
		return !(stringDate != null ? !stringDate.equals(news.stringDate) : news.stringDate != null);

	}

	@Override
	public int hashCode() {
		int result = newsId != null ? newsId.hashCode() : 0;
		result = 31 * result + (title != null ? title.hashCode() : 0);
		result = 31 * result + (shortText != null ? shortText.hashCode() : 0);
		result = 31 * result + (fullText != null ? fullText.hashCode() : 0);
		result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
		result = 31 * result + (modificationDate != null ? modificationDate.hashCode() : 0);
		result = 31 * result + (rownum != null ? rownum.hashCode() : 0);
		result = 31 * result + (stringDate != null ? stringDate.hashCode() : 0);
		return result;
	}
}