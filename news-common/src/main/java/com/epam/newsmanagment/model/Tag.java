package com.epam.newsmanagment.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "TAGS")
@NamedQuery(name="Tag.getAll", query="SELECT t FROM Tag t")
public class Tag {

	@Id
	@SequenceGenerator(name="tag_seq", initialValue=20, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="tag_seq")
	@Column(name = "TAG_ID")
	private Long tagId;

	@Size(min=1, max=30)
	@Column(name = "TAG_NAME", length = 30)
	private String tagName;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "tagList")
	@Fetch(value = FetchMode.SUBSELECT)
	private List<News> newsList;

	public Tag(Long tagId, String tagName) {
		super();
		this.tagId = tagId;
		this.tagName = tagName;
	}

	public Tag() {

	}

	public Tag(Long tagId) {
		this.tagId = tagId;
	}

	public Long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Tag tag = (Tag) o;

		if (tagId != null ? !tagId.equals(tag.tagId) : tag.tagId != null) return false;
		return !(tagName != null ? !tagName.equals(tag.tagName) : tag.tagName != null);

	}

	@Override
	public int hashCode() {
		int result = tagId != null ? tagId.hashCode() : 0;
		result = 31 * result + (tagName != null ? tagName.hashCode() : 0);
		return result;
	}
}
