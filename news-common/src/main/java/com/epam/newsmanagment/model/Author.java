package com.epam.newsmanagment.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

@Entity
@Table(name = "AUTHORS")
@NamedQuery(name="Author.getAll", query="SELECT a FROM Author a")
public class Author {

	@NotNull
	@Id
	@SequenceGenerator(name="seq", initialValue=20, allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
	@Column(name = "AUTHOR_ID")
	private Long authorId;

	@Size(min = 1, max=20)
	@Column(name = "AUTHOR_NAME", length = 20)
	private String authorName;

	@Column(name = "EXPIRED")
	private Timestamp expired;

	public Author() {
	}

	public Author(Long authorId, String authorName, Timestamp expired) {
		super();
		this.authorId = authorId;
		this.authorName = authorName;
		this.expired = expired;
	}

	public Author(String authorName) {
		this.authorName = authorName;
	}

	public Long getAuthorId() {
		return authorId;
	}
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public Timestamp getExpired() {
		return expired;
	}
	public void setExpired(Timestamp expired) {
		this.expired = expired;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Author author = (Author) o;

		if (authorId != null ? !authorId.equals(author.authorId) : author.authorId != null) return false;
		if (authorName != null ? !authorName.equals(author.authorName) : author.authorName != null) return false;
		return !(expired != null ? !expired.equals(author.expired) : author.expired != null);

	}

	@Override
	public int hashCode() {
		int result = authorId != null ? authorId.hashCode() : 0;
		result = 31 * result + (authorName != null ? authorName.hashCode() : 0);
		result = 31 * result + (expired != null ? expired.hashCode() : 0);
		return result;
	}
}
