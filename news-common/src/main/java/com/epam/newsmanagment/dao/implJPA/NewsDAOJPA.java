package com.epam.newsmanagment.dao.implJPA;

import com.epam.newsmanagment.dao.INewsDAO;
import com.epam.newsmanagment.dao.impl.NewsDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.model.News;
import com.epam.newsmanagment.model.SearchCriteria;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Arseni_Vorhan on 9/17/2015.
 */
public class NewsDAOJPA implements INewsDAO {

    private static final String JPQL_ORDER = " order by n.commentsCount desc, n.modificationDate asc";
    private static final String JPQL_SEARCH_COUNT_START = "select count(distinct n.newsId) from News n ";
    private static final String JPQL_SEARCH_START = "select distinct n from News n ";
//    @PersistenceContext(unitName = "localPU")
//    private EntityManager em;
public EntityManager em = Persistence.createEntityManagerFactory("localPU").createEntityManager();

    public List<News> searchNews(SearchCriteria searchCriteria) throws DAOException {
        StringBuilder query = new StringBuilder(JPQL_SEARCH_START);

//        Query createdQuery = em.createQuery(query.toString());



        Query createdQuery = buildSearchJPQL(searchCriteria, query, false);


        return createdQuery.setFirstResult((int) (searchCriteria.getMinCount() - 1))
                .setMaxResults((int) (searchCriteria.getMaxCount() - searchCriteria.getMinCount() + 1)).getResultList();

    }

    public Long countAllNews() throws DAOException {
//        return null;
        return 10L;
    }

    public Long countAllNews(SearchCriteria searchCriteria) throws DAOException {
        return Long.valueOf(searchNews(searchCriteria).size());
    }

    public void deleteNewsList(List<Long> newsIdList) throws DAOException {
        for (Long newsId : newsIdList) {
            em.remove(em.find(News.class, newsId));
        }
    }

    public Long getRownumByNewsId(SearchCriteria searchCriteria, Long newsId) throws DAOException {
//        Query query = em.createNativeQuery(buildQueryFetchIndexById(searchCriteria));
//        setIndexValues(query, searchCriteria, newsId);
//        return ((BigDecimal) query.getSingleResult()).longValue();
        List<News> newsList = searchNews(searchCriteria);
        for (int i = 0; i < newsList.size(); i++){
            if(newsList.get(i).getNewsId().equals(newsId)){
                return Long.valueOf(i + 1);
            }
        }
        return null;
    }

    public Long addItem(News entity) throws DAOException {
        entity.setNewsId(null);
        entity.setModificationDate(new Date(entity.getCreationDate().getTime()));
        em.persist(entity);
        return entity.getNewsId();
    }

    public void editItem(News entity) throws DAOException {
        entity.setModificationDate(new Date(new Timestamp(System.currentTimeMillis()).getTime()));
        em.merge(entity);
    }

    public void deleteItem(Long itemId) throws DAOException {
        em.remove(em.find(News.class, itemId));
    }

    public News readItem(Long itemId) throws DAOException {
//        return em.find(News.class, itemId);
               return em.createQuery(
                "select n from News n left join fetch n.tagList left join fetch n.commentList left join fetch n.authorItem where n.newsId = :newsId",
                News.class).setParameter("newsId", itemId).getSingleResult();
    }

    private Query buildSearchJPQL(SearchCriteria searchCriteria, StringBuilder query, boolean count) {
        Long criteriaAuthorId = searchCriteria.getAuthorId();
        List<Long> criteriaTagIds = searchCriteria.getTagIDs();
        boolean author = criteriaAuthorId != null && criteriaAuthorId != 0L;
        boolean tags = criteriaTagIds != null && !criteriaTagIds.isEmpty();

        query.append(" inner join n.authorItem a ");
//        if (tags) {
//            for (Long tagId : criteriaTagIds) {
//                query.append("inner join n.tagList t").append(tagId)
//                        .append(" on t").append(tagId).append(".tagId = ")
//                        .append(tagId).append(" ");
//            }
//        }
        if (author) {

            query.append(" where ");
            query.append(" n.authorItem.authorId = ").append(criteriaAuthorId).append(" ");
        }
        if (!count) {
            query.append(JPQL_ORDER);
        }
        Query createdQuery = em.createQuery(query.toString());
        return createdQuery;
    }

    public static void main(String[] args) {
        NewsDAOJPA newsDAOJPA = new NewsDAOJPA();
        SearchCriteria sc = new SearchCriteria();
//        sc.setAuthorId(1L);
        List<Long> tg = new ArrayList<Long>();
        tg.add(1L);
        sc.setTagIDs(tg);

        sc.setMinCount(1L);
        sc.setMaxCount(5L);
        try {
            List<News> newsList = newsDAOJPA.searchNews(sc);
            for(News news : newsList){
                System.out.println(news.getAuthorItem().getAuthorName());
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }


    }






}
