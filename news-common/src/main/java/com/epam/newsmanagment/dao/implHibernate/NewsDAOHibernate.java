package com.epam.newsmanagment.dao.implHibernate;

import com.epam.newsmanagment.dao.INewsDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.model.News;
import com.epam.newsmanagment.model.SearchCriteria;
import com.epam.newsmanagment.model.Tag;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.*;

import java.util.List;

/**
 * Created by Arseni_Vorhan on 9/17/2015.
 */
public class NewsDAOHibernate implements INewsDAO {

    private SessionFactory sessionFactory;

    public NewsDAOHibernate(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public Long getRownumByNewsId(SearchCriteria searchCriteria, Long newsId) throws DAOException {

        List<News> newsList = searchNews(searchCriteria);

        for (int i = 0; i < newsList.size(); i++){
            if(newsList.get(i).getNewsId().equals(newsId)){
                return Long.valueOf(i + 1);
            }
        }
        return null;
    }

    public List<News> searchNews(SearchCriteria searchCriteria) throws DAOException {

        Criteria criteria = buildSearchCriteria(searchCriteria);

        criteria.addOrder(Order.desc("commentsCount"));
        criteria.addOrder(Order.asc("modificationDate"));

        criteria.setFirstResult(searchCriteria.getMinCount().intValue() - 1);
        criteria.setMaxResults(searchCriteria.getMaxCount().intValue() - searchCriteria.getMinCount().intValue() + 1);

        return criteria.list();
    }

    public Long countAllNews() throws DAOException {
        Session session = null;
        Long count = null;
        try {
            session = getSession();
            count = (Long)session.createCriteria(News.class).setProjection(Projections.rowCount()).uniqueResult();
        } catch (Exception e) {
            throw new DAOException(e);
        }
        return count;
    }

    public Long countAllNews(SearchCriteria searchCriteria) throws DAOException {

        return (Long) buildSearchCriteria(searchCriteria).setProjection(Projections.rowCount()).uniqueResult();
    }

    public void deleteNewsList(List<Long> newsIdList) throws DAOException {
        Session session = null;
        try {
            session = getSession();
            for(Long newsId : newsIdList) {
                News news = (News) session.get(News.class, newsId);
                session.delete(news);
            }

        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    public Long addItem(News news) throws DAOException {
        Session session = null;
        try {
            session = getSession();
            session.save(news);

        } catch (Exception e) {
            throw new DAOException(e);
        }
        return news.getNewsId();
    }

    public void editItem(News news) throws DAOException {
        Session session = null;
        try {
            session = getSession();
            session.update(news);

        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    public void deleteItem(Long newsId) throws DAOException {
        Session session = null;
        try {
            session = getSession();
            News news = (News)session.get(News.class, newsId);
            session.delete(news);

        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    public News readItem(Long newsId) throws DAOException {
        Session session = null;
        News news = null;
        try {
            session = getSession();
            news = (News) session.get(News.class, newsId);

        } catch (Exception e) {
            throw new DAOException(e);
        }
        return news;
    }

    public Criteria buildSearchCriteria(SearchCriteria searchCriteria) throws DAOException {
        try {
            Session session = null;
            session = getSession();
            Criteria criteria = session.createCriteria(News.class, "newsManagementVO");

            if(null != searchCriteria) {

                if (null != searchCriteria.getAuthorId()) {
                    criteria.add(Restrictions.eq("authorItem.authorId", searchCriteria.getAuthorId()));
                }

                if (null != searchCriteria.getTagIDs()) {
                    for (Long tagId : searchCriteria.getTagIDs()) {
                        DetachedCriteria subquery = DetachedCriteria.forClass(Tag.class, "tag");
                        subquery.add(Restrictions.eq("tagId", tagId));

                        subquery.setProjection(Projections.property("tagId"));
                        subquery.createAlias("newsList", "news");

                        subquery.add(Restrictions.eqProperty("news.newsId", "newsManagementVO.newsId"));
                        criteria.add(Subqueries.exists(subquery));
                    }
                }
            }
            return criteria;
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }
}
