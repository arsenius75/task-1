package com.epam.newsmanagment.dao;

import com.epam.newsmanagment.model.User;

public interface IUserDAO extends ICrudDAO<User>{

}
