package com.epam.newsmanagment.dao;

import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.model.Author;

import java.util.List;

public interface IAuthorDAO extends ICrudDAO<Author> {

	void link(Long authorId, Long newsId) throws DAOException;

	void unlink(Long newsId) throws DAOException;

	Author getAuthorOfNews(Long newsId) throws DAOException;

	List<Author> getAllAuthors() throws DAOException;

	void expireAuthor(Long authorId) throws DAOException;
}
