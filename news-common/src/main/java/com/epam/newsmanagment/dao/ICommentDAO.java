package com.epam.newsmanagment.dao;

import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.model.Comment;

import java.util.List;

public interface ICommentDAO extends ICrudDAO<Comment> {

    List<Comment> getAllCommentsForNews(Long newsId) throws DAOException;

    Long getCommentsCountForNews(Long newsId) throws DAOException;

    void deleteAllCommentsOfNews(Long newsId) throws DAOException;
}
