package com.epam.newsmanagment.dao;

import com.epam.newsmanagment.exception.DAOException;

public interface ICrudDAO <E>{

	Long addItem(E entity) throws DAOException;
	
	void editItem(E entity) throws DAOException;
	
	void deleteItem(Long itemId) throws DAOException;
	
	E readItem(Long itemId) throws DAOException;
	
}
