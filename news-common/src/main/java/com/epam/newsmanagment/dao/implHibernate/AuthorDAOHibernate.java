package com.epam.newsmanagment.dao.implHibernate;

import com.epam.newsmanagment.dao.IAuthorDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.model.Author;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

public class AuthorDAOHibernate implements IAuthorDAO {

    private SessionFactory sessionFactory;

    public AuthorDAOHibernate(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public void link(Long authorId, Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public void unlink(Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public Author getAuthorOfNews(Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public List<Author> getAllAuthors() throws DAOException {
        Session session = null;
        List<Author> authors = null;
        try {
            session = getSession();
            authors = session.createCriteria(Author.class).list();
        } catch (Exception e) {
            throw new DAOException(e);
        }
        return authors;
    }

    public void expireAuthor(Long authorId) throws DAOException {
        Session session = getSession();
//        try {
            Author author = (Author) session.get(Author.class, authorId);
            author.setExpired(Timestamp.from(Instant.now()));
            session.update(author);
//        } catch (Exception e) {
//            throw new DAOException(e);
//        }
    }

    public Long addItem(Author author) throws DAOException {
        Session session = null;
        try {
            session = getSession();
            session.save(author);
        } catch (Exception e) {
            throw new DAOException(e);
        }
        return author.getAuthorId();
    }

    public void editItem(Author author) throws DAOException {
        Session session = null;
        try {
            session = getSession();
            session.update(author);
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    public void deleteItem(Long itemId) throws DAOException {

    }

    public Author readItem(Long itemId) throws DAOException {
        return null;
    }
}
