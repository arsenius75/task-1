package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.dao.ITagDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.model.Tag;
import com.epam.newsmanagment.util.DBCloser;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arseni on 7/20/2015.
 */
public class TagDAO implements ITagDAO {

    private static final String ADD_TAG = "INSERT INTO tags (tag_id, tag_name) VALUES (sequence_tags_id.nextval, ?)";
    private static final String EDIT_TAG = "UPDATE tags SET tag_name= ? WHERE tag_id = ?";
    private static final String DELETE_TAG= "DELETE FROM tags WHERE tag_id = ?";
    private static final String READ_TAG = "SELECT tag_id, tag_name FROM tags WHERE tag_id= ?";
    private static final String READ_All_TAGS= "SELECT tag_id, tag_name FROM tags";

    private static final String LINK_NEWS_TAG = "INSERT INTO news_tags (news_id, tag_id) VALUES (?,?)";
    private static final String UNLINK_NEWS_TAG = "DELETE FROM news_tags WHERE news_id=? AND tag_id=?";
    private static final String READ_TAGS_OF_NEWS = "SELECT tags.tag_id, tag_name FROM tags JOIN news_tags ON tags.tag_id = news_tags.tag_id WHERE news_tags.news_id = ?";

    private static final String UNLINK_TAGS = "DELETE FROM news_tags WHERE news_id = ?";
    private static final String UNLINK_TAG = "DELETE FROM news_tags WHERE tag_id = ?";

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Long addItem(Tag entity) throws DAOException {

        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        Long tagId = null;
        String[] column = {"TAG_ID"};

        try {
            connection = DataSourceUtils.getConnection(dataSource);

            preparedStatement = connection.prepareStatement(ADD_TAG, column);
            preparedStatement.setString(1, entity.getTagName());
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();
            if ((resultSet != null) && (resultSet.next())) {
                tagId = resultSet.getLong(1);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
        }
        return tagId;
    }

    public void editItem(Tag entity) throws DAOException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.getConnection(dataSource);
            preparedStatement = connection.prepareStatement(EDIT_TAG);
            preparedStatement.setString(1, entity.getTagName());
            preparedStatement.setLong(2, entity.getTagId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
        }
    }

    public void deleteItem(Long itemId) throws DAOException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;

        try {
            connection = DataSourceUtils.getConnection(dataSource);
            preparedStatement = connection.prepareStatement(DELETE_TAG);
            preparedStatement.setLong(1, itemId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
        }
    }

    public Tag readItem(Long itemId) throws DAOException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        Tag tag = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            preparedStatement = connection.prepareStatement(READ_TAG);
            preparedStatement.setLong(1, itemId);

            resultSet = preparedStatement.executeQuery();
            if ((resultSet != null) && (resultSet.next())) {
                Long tagId = resultSet.getLong(1);
                String tagName = resultSet.getString(2);

                tag = new Tag(tagId, tagName);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
        }
        return tag;
    }

    public void link(Long tagId, Long newsId) throws DAOException {

        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.getConnection(dataSource);

            preparedStatement = connection.prepareStatement(LINK_NEWS_TAG);
            preparedStatement.setLong(1, newsId);
            preparedStatement.setLong(2, tagId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
        }
    }

    public void unlink(Long tagId, Long newsId) throws DAOException {

        PreparedStatement preparedStatement = null;
        Connection connection = null;

        try {
            connection = DataSourceUtils.getConnection(dataSource);
            preparedStatement = connection.prepareStatement(UNLINK_NEWS_TAG);
            preparedStatement.setLong(1, newsId);
            preparedStatement.setLong(2, tagId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
        }
    }


    public List<Tag> getAllTagsOfNews(Long newsId) throws DAOException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        List<Tag> tags = new ArrayList<Tag>();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            preparedStatement = connection.prepareStatement(READ_TAGS_OF_NEWS);
            preparedStatement.setLong(1, newsId);

            resultSet = preparedStatement.executeQuery();
            while((resultSet != null) && (resultSet.next())) {
                Long tagId = resultSet.getLong(1);
                String tagName = resultSet.getString(2);

                Tag tag = new Tag(tagId, tagName);
                tags.add(tag);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
        }
        return tags;
    }

    public void unlinkAllTagsOfNews(Long newsId) throws DAOException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;

        try {
            connection = DataSourceUtils.getConnection(dataSource);
            preparedStatement = connection.prepareStatement(UNLINK_TAGS);
            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
        }
    }

    public void linkTagsToNews(List<Long> tagIdList, Long newsId) throws DAOException {

        PreparedStatement preparedStatement = null;
        Connection connection = null;

        try {
            connection = DataSourceUtils.getConnection(dataSource);
//            connection.setAutoCommit(false);

            int i = 0;
            for(Long tagId : tagIdList) {
                preparedStatement = connection.prepareStatement(LINK_NEWS_TAG);
                preparedStatement.setLong(1, newsId);
                preparedStatement.setLong(2, tagIdList.get(i));
                preparedStatement.addBatch();
                i++;
            }

            preparedStatement.executeBatch();
//            connection.setAutoCommit(true);

//            connection.commit();


        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
        }
    }

    public List<Tag> getAllTags() throws DAOException {

        Statement statement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        List<Tag> tags = new ArrayList<Tag>();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(READ_All_TAGS);

            while((resultSet != null) && (resultSet.next())) {
                Long tagId = resultSet.getLong(1);
                String tagName = resultSet.getString(2);

                Tag tag = new Tag(tagId, tagName);
                tags.add(tag);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, statement, resultSet);
        }
        return tags;
    }

    public void unlinkTagFromAllNews(Long tagId) throws DAOException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;

        try {
            connection = DataSourceUtils.getConnection(dataSource);
            preparedStatement = connection.prepareStatement(UNLINK_TAG);
            preparedStatement.setLong(1, tagId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
        }
    }
}