package com.epam.newsmanagment.dao;

import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.model.News;
import com.epam.newsmanagment.model.SearchCriteria;

import java.util.List;


public interface INewsDAO extends ICrudDAO<News> {

	List<News> searchNews(SearchCriteria searchCriteria) throws DAOException;

	Long countAllNews() throws DAOException;

	Long countAllNews(SearchCriteria searchCriteria) throws DAOException;


	void deleteNewsList(List<Long> newsIdList) throws DAOException;

	Long getRownumByNewsId(SearchCriteria searchCriteria, Long newsId) throws DAOException;
}
