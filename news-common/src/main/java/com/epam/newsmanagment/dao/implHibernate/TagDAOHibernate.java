package com.epam.newsmanagment.dao.implHibernate;

import com.epam.newsmanagment.dao.ITagDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.model.Tag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;


public class TagDAOHibernate implements ITagDAO {

    private SessionFactory sessionFactory;

    public TagDAOHibernate(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public void link(Long tagId, Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public void unlink(Long tagId, Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public List<Tag> getAllTagsOfNews(Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public void unlinkAllTagsOfNews(Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public void linkTagsToNews(List<Long> tagIdList, Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public void unlinkTagFromAllNews(Long tagId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public List<Tag> getAllTags() throws DAOException {
        Session session = null;
        List<Tag> authors = null;
        try {
            session = getSession();
            authors = session.createCriteria(Tag.class).list();
        } catch (Exception e) {
            throw new DAOException(e);
        }
        return authors;
    }

    public Long addItem(Tag tag) throws DAOException {
        Session session = null;
        try {
            session = getSession();
            session.save(tag);
        } catch (Exception e) {
            throw new DAOException(e);
        }
        return tag.getTagId();
    }

    public void editItem(Tag tag) throws DAOException {
        Session session = null;
        try {
            session = getSession();
            session.update(tag);
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    public void deleteItem(Long tagId) throws DAOException {
            Session session = null;
            try {



                session = getSession();
                session.delete(session.get(Tag.class, tagId));

            } catch (Exception e) {
                throw new DAOException(e);
            }
    }

    public Tag readItem(Long tagId) throws DAOException {
        Session session = null;
        Tag tag = null;
        try {
            session = getSession();
            tag = (Tag) session.get(Tag.class, tagId);
        } catch (Exception e) {
            throw new DAOException(e);
        }
        return tag;
    }
}
