package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.dao.IAuthorDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.model.Author;
import com.epam.newsmanagment.util.DBCloser;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AuthorDAO implements IAuthorDAO {
	private static final String ADD_AUTHOR = "INSERT INTO authors (author_id, author_name, expired) VALUES (sequence_author_id.nextval, ? , null)";
	private static final String EDIT_AUTHOR = "UPDATE authors SET author_name= ?, expired=current_timestamp WHERE author_id = ?";
	private static final String DELETE_AUTHOR= "DELETE FROM authors WHERE author_id = ?";
	private static final String READ_AUTHOR = "SELECT author_id, author_name, expired FROM authors WHERE author_id= ?";

	private static final String LINK_NEWS_AUTHOR = "INSERT INTO news_authors (news_id, author_id) VALUES (?,?)";
	private static final String UNLINK_NEWS_AUTHOR = "DELETE FROM news_authors WHERE news_id=?";
	private static final String READ_AUTHOR_OF_NEWS = "SELECT author_id, author_name, expired FROM authors WHERE author_id = (SELECT author_id FROM news JOIN news_authors ON news_authors.news_id = news.news_id WHERE news.news_id = ?)";
	private static final String READ_AUTHORS = "SELECT author_id, author_name, expired FROM authors";

	private static final String EXPIRE_AUTHOR = "UPDATE authors SET expired = current_timestamp where author_id = ?";


	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Long addItem(Author entity) throws DAOException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		Long authorId = null;
		String[] column = {"AUTHOR_ID"};
		
		try {
			connection = DataSourceUtils.getConnection(dataSource);

			preparedStatement = connection.prepareStatement(ADD_AUTHOR, column);
			preparedStatement.setString(1, entity.getAuthorName());
			preparedStatement.executeUpdate();
			
			resultSet = preparedStatement.getGeneratedKeys();
			if ((resultSet != null) && (resultSet.next())) {
				authorId = resultSet.getLong(1);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
		}
		return authorId;
			
	}

	public void editItem(Author entity) throws DAOException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(EDIT_AUTHOR);
			preparedStatement.setString(1, entity.getAuthorName());
            preparedStatement.setLong(2, entity.getAuthorId());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
		}
	}

	public void deleteItem(Long itemId) throws DAOException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(DELETE_AUTHOR);
			preparedStatement.setLong(1, itemId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}
	}

	public Author readItem(Long itemId) throws DAOException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		Author author = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(READ_AUTHOR);
			preparedStatement.setLong(1, itemId);

			resultSet = preparedStatement.executeQuery();
			if ((resultSet != null) && (resultSet.next())) {
				Long authorId = resultSet.getLong(1);
				String authorName = resultSet.getString(2);
				Timestamp expired = resultSet.getTimestamp(3);
				author =  new Author(authorId, authorName, expired);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
		}
		return author;
	}

	public void link(Long authorId, Long newsId) throws DAOException {

		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);

			preparedStatement = connection.prepareStatement(LINK_NEWS_AUTHOR);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
		}
	}

	public void unlink(Long newsId) throws DAOException {

		PreparedStatement preparedStatement = null;
		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(UNLINK_NEWS_AUTHOR);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
		}
	}

	public Author getAuthorOfNews(Long newsId) throws DAOException {
		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		Author author = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(READ_AUTHOR_OF_NEWS);
			preparedStatement.setLong(1, newsId);

			resultSet = preparedStatement.executeQuery();
			if ((resultSet != null) && (resultSet.next())) {
				Long authorId = resultSet.getLong(1);
				String authorName = resultSet.getString(2);
				Timestamp expired = resultSet.getTimestamp(3);
				author =  new Author(authorId, authorName, expired);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
		}
		return author;
	}

	public List<Author> getAllAuthors() throws DAOException {
		Statement statement = null;
		Connection connection = null;
		ResultSet resultSet = null;
		List<Author> authors = new ArrayList<Author>();
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();

			resultSet = statement.executeQuery(READ_AUTHORS);

			while ((resultSet != null) && (resultSet.next())) {
				Long authorId = resultSet.getLong(1);
				String authorName = resultSet.getString(2);
				Timestamp expired = resultSet.getTimestamp(3);
				Author author =  new Author(authorId, authorName, expired);
				authors.add(author);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, statement, resultSet);
		}
		return authors;
	}

	public void expireAuthor(Long authorId) throws DAOException {

		PreparedStatement preparedStatement = null;
		Connection connection = null;
		ResultSet resultSet = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(EXPIRE_AUTHOR);
			preparedStatement.setLong(1, authorId);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
		}
	}
}