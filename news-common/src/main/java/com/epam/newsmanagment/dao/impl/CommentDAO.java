package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.dao.ICommentDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.model.Comment;
import com.epam.newsmanagment.util.DBCloser;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CommentDAO implements ICommentDAO {

    private static final String ADD_COMMENT = "INSERT INTO comments (comment_id, news_id, comment_text, creation_date) VALUES (sequence_comment_id.nextval, ?, ?, current_timestamp)";
    private static final String EDIT_COMMENT = "UPDATE comments SET comment_text=?,  creation_date=current_timestamp WHERE comment_id = ?";
    private static final String DELETE_COMMENT= "DELETE FROM comments WHERE comment_id = ?";
    private static final String READ_COMMENT = "SELECT comment_id, news_id, comment_text, creation_date FROM comments WHERE comment_id= ?";
    private static final String READ_COMMENTS = "SELECT comment_id, comment_text, creation_date FROM comments WHERE news_id= ? ORDER BY creation_date ASC";
    private static final String DELETE_COMMENTS = "DELETE FROM comments WHERE news_id = ?";
    private static final String COUNT_COMMENTS = "SELECT count(*) FROM comments WHERE news_id = ?";

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Long addItem(Comment entity) throws DAOException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        Long commentId = null;
        String[] column = {"COMMENT_ID"};

        try {
            connection = DataSourceUtils.getConnection(dataSource);

            preparedStatement = connection.prepareStatement(ADD_COMMENT, column);
            preparedStatement.setLong(1, entity.getNewsId());
            preparedStatement.setString(2, entity.getCommentText());
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();
            if ((resultSet != null) && (resultSet.next())) {
                commentId = resultSet.getLong(1);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
        }
        return commentId;
    }

    public void editItem(Comment entity) throws DAOException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.getConnection(dataSource);
            preparedStatement = connection.prepareStatement(EDIT_COMMENT);
            preparedStatement.setString(1, entity.getCommentText());
            preparedStatement.setLong(2, entity.getCommentId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
        }
    }

    public void deleteItem(Long itemId) throws DAOException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;

        try {
            connection = DataSourceUtils.getConnection(dataSource);
            preparedStatement = connection.prepareStatement(DELETE_COMMENT);
            preparedStatement.setLong(1, itemId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
        }
    }

    public Comment readItem(Long itemId) throws DAOException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        Comment comment = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            preparedStatement = connection.prepareStatement(READ_COMMENT);
            preparedStatement.setLong(1, itemId);

            resultSet = preparedStatement.executeQuery();
            if ((resultSet != null) && (resultSet.next())) {
                Long commentId = resultSet.getLong(1);
                Long newsId = resultSet.getLong(2);
                String commentText = resultSet.getString(3);
                Timestamp expired = resultSet.getTimestamp(4);
                comment =  new Comment(commentId, newsId, commentText, expired);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
        }
        return comment;
    }

    public List<Comment> getAllCommentsForNews(Long newsId) throws DAOException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        List<Comment> comments = new ArrayList<Comment>();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            preparedStatement = connection.prepareStatement(READ_COMMENTS);
            preparedStatement.setLong(1, newsId);

            resultSet = preparedStatement.executeQuery();
            while ((resultSet != null) && (resultSet.next())) {
                Long commentId = resultSet.getLong(1);
                String commentText = resultSet.getString(2);
                Timestamp creationDate = resultSet.getTimestamp(3);
                Comment comment = new Comment(commentId, newsId, commentText, creationDate);
                comments.add(comment);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
        }
        return comments;
    }

    public Long getCommentsCountForNews(Long newsId) throws DAOException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;

        Long count = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);

            preparedStatement = connection.prepareStatement(COUNT_COMMENTS);
            preparedStatement.setLong(1, newsId);
            resultSet = preparedStatement.executeQuery();

            if ((resultSet != null) && (resultSet.next())) {
                count = resultSet.getLong(1);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
        }
        return count;
    }

    public void deleteAllCommentsOfNews(Long newsId) throws DAOException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;

        try {
            connection = DataSourceUtils.getConnection(dataSource);
            preparedStatement = connection.prepareStatement(DELETE_COMMENTS);
            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
        }
    }
}