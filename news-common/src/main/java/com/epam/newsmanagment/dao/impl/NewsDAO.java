package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.dao.INewsDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.model.News;
import com.epam.newsmanagment.model.SearchCriteria;
import com.epam.newsmanagment.util.DBCloser;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arseni on 7/20/2015.
 */
public class NewsDAO implements INewsDAO {

    private static final String ADD_NEWS = "INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date) " +
            "VALUES (sequence_news_id.nextval, ?, ?, ?, ? , sysdate)";
    private static final String EDIT_NEWS = "UPDATE news SET title=?, short_text=?, full_text=?, creation_date=?, modification_date=sysdate WHERE news_id = ?";
    private static final String DELETE_NEWS= "DELETE FROM news WHERE news_id = ?";
    private static final String READ_NEWS = "SELECT news_id, title, short_text, full_text, creation_date, modification_date FROM news WHERE news_id= ?";
    private static final String SHOW_NEWS_COUNT = "SELECT COUNT(DISTINCT news_id) FROM news";

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Long addItem(News entity) throws DAOException {

        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        Long newsId = null;
        String[] column = {"NEWS_ID"};

        try {
            connection = DataSourceUtils.getConnection(dataSource);

            preparedStatement = connection.prepareStatement(ADD_NEWS, column);
            preparedStatement.setString(1, entity.getTitle());
            preparedStatement.setString(2, entity.getShortText());
            preparedStatement.setString(3, entity.getFullText());
            preparedStatement.setTimestamp(4, entity.getCreationDate());
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();
            if ((resultSet != null) && (resultSet.next())) {
                newsId = resultSet.getLong(1);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
        }
        return newsId;
    }

    public void editItem(News entity) throws DAOException {

        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.getConnection(dataSource);
            preparedStatement = connection.prepareStatement(EDIT_NEWS);
            preparedStatement.setString(1, entity.getTitle());
            preparedStatement.setString(2, entity.getShortText());
            preparedStatement.setString(3, entity.getFullText());
            preparedStatement.setTimestamp(4, entity.getCreationDate());
            preparedStatement.setLong(5, entity.getNewsId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
        }
    }

    public void deleteItem(Long itemId) throws DAOException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;

        try {
            connection = DataSourceUtils.getConnection(dataSource);
            preparedStatement = connection.prepareStatement(DELETE_NEWS);
            preparedStatement.setLong(1, itemId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement);
        }
    }

    public News readItem(Long itemId) throws DAOException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        News news = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            preparedStatement = connection.prepareStatement(READ_NEWS);
            preparedStatement.setLong(1, itemId);

            resultSet = preparedStatement.executeQuery();

            if ((resultSet != null) && (resultSet.next())) {
                Long newsId = resultSet.getLong(1);
                String title = resultSet.getString(2);
                String shortText = resultSet.getString(3);
                String fullText = resultSet.getString(4);
                Timestamp creationDate = resultSet.getTimestamp(5);
                Date modificationDate = resultSet.getDate(6);

                news = new News(newsId, title, shortText, fullText, creationDate, modificationDate);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
        }
        return news;
    }


    public Long countAllNews() throws DAOException {
        Statement statement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        Long totalNewsCount = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SHOW_NEWS_COUNT);
            if ((resultSet != null) && (resultSet.next())) {
                totalNewsCount = resultSet.getLong(1);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, statement,
                    resultSet);
        }
        return totalNewsCount;
    }

    public List<News> searchNews(SearchCriteria searchCriteria) throws DAOException {

        StringBuilder querryString = new StringBuilder("select news_id, title, short_text, full_text, creation_date, modification_date, rnum from ( select rownum rnum, a.*  from ( SELECT news_id, title, short_text, full_text, creation_date, modification_date FROM news second_news ");
        List<Long> tagIdList = null;
        Long authorId = null;

        if(null != searchCriteria) {

            tagIdList = searchCriteria.getTagIDs();
            authorId = searchCriteria.getAuthorId();

            querryString.append(addAuthorCriteria(searchCriteria));
            querryString.append(addTagCriteria(searchCriteria));

            if(null != authorId) {
                querryString.append(")");
            }
            querryString.append(" ORDER BY (SELECT COUNT(COMMENT_ID) FROM NEWS Selected_News LEFT OUTER JOIN COMMENTS ON COMMENTS.NEWS_ID = Selected_News.NEWS_ID where Selected_News.news_id = second_news.news_id Group BY Selected_News.news_id ) DESC, second_news.MODIFICATION_DATE ASC\n" );
            querryString.append(") a  where rownum <= ? ) where rnum >= ?");
        }

        List<News> newsList = new ArrayList<News>();
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        News news = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);

            preparedStatement = connection.prepareStatement(querryString.toString());

            int i = 1;
            if(null != authorId) {
                preparedStatement.setLong(i, authorId);
                i++;
            }
            if(null != tagIdList && !tagIdList.isEmpty()) {
                for (Long tagId : tagIdList) {
                    if(null !=tagId) {
                        preparedStatement.setLong(i, tagId);
                        i++;
                    }
                }
            }


            if (searchCriteria != null) {
                preparedStatement.setLong(i, searchCriteria.getMaxCount());
                i++;
                preparedStatement.setLong(i, searchCriteria.getMinCount());
            }
            resultSet = preparedStatement.executeQuery();

            while ((resultSet != null) && (resultSet.next())) {
                Long newsId = resultSet.getLong(1);
                String title = resultSet.getString(2);
                String shortText = resultSet.getString(3);
                String fullText = resultSet.getString(4);
                Timestamp creationDate = resultSet.getTimestamp(5);
                Date modificationDate = resultSet.getDate(6);

                Long rownum = resultSet.getLong(7);
                news = new News(newsId, title, shortText, fullText, creationDate, modificationDate, rownum);
                newsList.add(news);
            }

        } catch (SQLException e) {
            System.out.println(querryString);
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
        }
        return newsList;

    }

    private StringBuilder addTagCriteria(SearchCriteria searchCriteria) {
        List<Long> tagIdList = searchCriteria.getTagIDs();
        Long authorId = searchCriteria.getAuthorId();
        StringBuilder stringBuilder = new StringBuilder();

        String searchByTag = " select news.news_id from news JOIN news_tags ON news.news_id = news_tags.news_id where tag_id = ? ";
        String connector = " AND news.news_id IN (";

        if(null != tagIdList && !tagIdList.isEmpty()) {

            if (null != authorId) {
                stringBuilder.append(connector);
            } else {
                stringBuilder.append(" WHERE news_id IN ( ");
            }

            int u = 1;
            int emptyTagId = 0;
            for (Long tagId : tagIdList) {
                if(null != tagId) {
                    stringBuilder.append(searchByTag);
                    if (u != (tagIdList.size()-emptyTagId)) {
                        stringBuilder.append(connector);
                    }
                    u++;
                }else{
                    emptyTagId++;
                }
            }

            for (Long tagId : tagIdList) {
                if(null != tagId) {
                    stringBuilder.append(")");
                }
            }
        }
        return stringBuilder;
    }

    private StringBuilder addAuthorCriteria(SearchCriteria searchCriteria) {

        StringBuilder stringBuilder = new StringBuilder();

        Long authorId = searchCriteria.getAuthorId();

        if (null != authorId) {
            stringBuilder.append(" WHERE news_id IN ( ");

            String searchByAuthor = "select news.news_id from news JOIN news_authors ON news.news_id = news_authors.news_id where author_id = ?";
            stringBuilder.append(searchByAuthor);
        }
        return stringBuilder;
    }

    public Long countAllNews(SearchCriteria searchCriteria) throws DAOException {


        Long totalNewsCount = null;
        StringBuilder querryString = new StringBuilder("SELECT COUNT(DISTINCT news_id) from ( SELECT news_id, title, short_text, full_text, creation_date, modification_date FROM news ");
        List<Long> tagIdList = null;
        Long authorId = null;

        if (null != searchCriteria) {

            tagIdList = searchCriteria.getTagIDs();
            authorId = searchCriteria.getAuthorId();

            querryString.append(addAuthorCriteria(searchCriteria));
            querryString.append(addTagCriteria(searchCriteria));

            if (null != authorId) {
                querryString.append(")");
            }
            querryString.append(")");
        }

        List<News> newsList = new ArrayList<News>();
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        News news = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);

            System.out.println(querryString);
            preparedStatement = connection.prepareStatement(querryString.toString());

            int i = 1;
            if (null != authorId) {
                preparedStatement.setLong(i, authorId);
                i++;
            }
            if (null != tagIdList && !tagIdList.isEmpty()) {
                for (Long tagId : tagIdList) {
                    if(null != tagId) {
                        preparedStatement.setLong(i, tagId);
                        i++;
                    }
                }
            }

            resultSet = preparedStatement.executeQuery();

            if ((resultSet != null) && (resultSet.next())) {
                totalNewsCount = resultSet.getLong(1);
            }

        } catch (SQLException e) {
            System.out.println(querryString);
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
        }
        return totalNewsCount;
    }



    public void deleteNewsList(List<Long> newsIdList) throws DAOException {

        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            connection = DataSourceUtils.getConnection(dataSource);
            connection.setAutoCommit(false);

            int i = 0;
            for(Long tagId : newsIdList) {
                preparedStatement = connection.prepareStatement(DELETE_NEWS);
                preparedStatement.setLong(1, newsIdList.get(i));
                preparedStatement.addBatch();
                i++;
            }

            connection.setAutoCommit(true);
            preparedStatement.executeBatch();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
        }
    }

    public Long getRownumByNewsId(SearchCriteria searchCriteria, Long newsId) throws DAOException {
        StringBuilder querryString = new StringBuilder("select rnum from ( select rownum rnum, a.*  from ( SELECT news_id, title, short_text, full_text, creation_date, modification_date FROM news second_news");
        List<Long> tagIdList = null;
        Long authorId = null;

        if(null != searchCriteria) {

            tagIdList = searchCriteria.getTagIDs();
            authorId = searchCriteria.getAuthorId();

            querryString.append(addAuthorCriteria(searchCriteria));
            querryString.append(addTagCriteria(searchCriteria));

            if(null != authorId) {
                querryString.append(")");
            }
            querryString.append(" ORDER BY (SELECT COUNT(COMMENT_ID) FROM NEWS Selected_News LEFT OUTER JOIN COMMENTS ON COMMENTS.NEWS_ID = Selected_News.NEWS_ID where Selected_News.news_id = second_news.news_id Group BY Selected_News.news_id ) DESC, second_news.MODIFICATION_DATE ASC\n" );
//            querryString.append(" ORDER BY (SELECT COUNT(DISTINCT COMMENTS.COMMENT_ID) FROM COMMENTS where COMMENTS.NEWS_ID = news.NEWS_ID) DESC, news.MODIFICATION_DATE ASC");
//                    " ORDER BY modification_date ASC, (SELECT COUNT(DISTINCT COMMENTS.COMMENT_ID) FROM COMMENTS where COMMENTS.NEWS_ID = news.NEWS_ID) DESC");
            querryString.append(") a  where rownum <= ? ) where rnum >= ?");
            querryString.append(" AND news_id = ?");
        }

        List<News> newsList = new ArrayList<News>();
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        News news = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);

            preparedStatement = connection.prepareStatement(querryString.toString());

            int i = 1;
            if(null != authorId) {
                preparedStatement.setLong(i, authorId);
                i++;
            }
            if(null != tagIdList && !tagIdList.isEmpty()) {
                for (Long tagId : tagIdList) {
                    if(null !=tagId) {
                        preparedStatement.setLong(i, tagId);
                        i++;
                    }
                }
            }


            preparedStatement.setLong(i, searchCriteria.getMaxCount());
            i++;
            preparedStatement.setLong(i, searchCriteria.getMinCount());
            i++;
            preparedStatement.setLong(i, newsId);

            resultSet = preparedStatement.executeQuery();

            if ((resultSet != null) && (resultSet.next())) {
                Long rownum = resultSet.getLong(1);
                return rownum;
            }

        } catch (SQLException e) {
            System.out.println(querryString);
            throw new DAOException(e);
        } finally {
            DBCloser.dataBaseCloser(dataSource, connection, preparedStatement, resultSet);
        }
        return null;

    }
}

