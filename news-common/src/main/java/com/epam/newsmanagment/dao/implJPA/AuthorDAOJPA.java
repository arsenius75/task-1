package com.epam.newsmanagment.dao.implJPA;

import com.epam.newsmanagment.dao.IAuthorDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.model.Author;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class AuthorDAOJPA implements IAuthorDAO {

//    @PersistenceContext(unitName = "localPU")
//    private EntityManager em;
public EntityManager em = Persistence.createEntityManagerFactory("localPU").createEntityManager();

    public void link(Long authorId, Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public void unlink(Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public Author getAuthorOfNews(Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public List<Author> getAllAuthors() throws DAOException {
        TypedQuery<Author> namedQuery = em.createNamedQuery("Author.getAll", Author.class);
        return namedQuery.getResultList();
    }

    public void expireAuthor(Long authorId) throws DAOException {
        em.getTransaction().begin();
        Author author = em.find(Author.class, authorId);

//        author.setExpired();
        //TODO MAKE EXPIRED
        em.merge(author);
        em.getTransaction().commit();
    }

    //TODO set any timestamp
    public Long addItem(Author author) throws DAOException {

        em.getTransaction().begin();
        em.persist(author);
        em.getTransaction().commit();
        return author.getAuthorId();
    }

    public void editItem(Author author) throws DAOException {
        em.getTransaction().begin();
        em.merge(author);
        em.getTransaction().commit();
    }

    public void deleteItem(Long authorId) throws DAOException {
        em.getTransaction().begin();
        Author author = em.find(Author.class, authorId);
        em.remove(author);
        em.getTransaction().commit();
    }

    public Author readItem(Long authorId) throws DAOException {
        Author author = null;
        em.getTransaction().begin();
        author = em.find(Author.class, authorId);
        em.getTransaction().commit();
        return author;
    }
}
