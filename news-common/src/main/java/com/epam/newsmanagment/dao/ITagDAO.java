package com.epam.newsmanagment.dao;

import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.model.Tag;

import java.util.List;

public interface ITagDAO extends ICrudDAO<Tag> {

	public void link(Long tagId, Long newsId) throws DAOException;

	public void unlink(Long tagId, Long newsId) throws DAOException;

	public List<Tag> getAllTagsOfNews(Long newsId) throws DAOException;

	public void unlinkAllTagsOfNews(Long newsId) throws DAOException;

	public void linkTagsToNews(List<Long> tagIdList, Long newsId) throws DAOException;

	List<Tag> getAllTags() throws DAOException;

	void unlinkTagFromAllNews(Long tagId) throws DAOException;
}
