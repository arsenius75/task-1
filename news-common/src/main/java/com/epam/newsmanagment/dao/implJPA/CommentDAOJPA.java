package com.epam.newsmanagment.dao.implJPA;

import com.epam.newsmanagment.dao.ICommentDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.model.Comment;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Arseni_Vorhan on 9/17/2015.
 */
public class CommentDAOJPA implements ICommentDAO {

//    @PersistenceContext(unitName = "localPU")
//    private EntityManager em;
public EntityManager em = Persistence.createEntityManagerFactory("localPU").createEntityManager();

    public List<Comment> getAllCommentsForNews(Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public Long getCommentsCountForNews(Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public void deleteAllCommentsOfNews(Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public Long addItem(Comment comment) throws DAOException {
        em.getTransaction().begin();
        em.persist(comment);
        em.getTransaction().commit();
        return comment.getCommentId();
    }

    public void editItem(Comment comment) throws DAOException {
        em.getTransaction().begin();
        em.merge(comment);
        em.getTransaction().commit();
    }

    public void deleteItem(Long commentId) throws DAOException {
        em.getTransaction().begin();
        Comment comment = em.find(Comment.class, commentId);
        em.remove(comment);
        em.getTransaction().commit();
    }

    public Comment readItem(Long commentId) throws DAOException {
        Comment comment = null;
        em.getTransaction().begin();
        comment = em.find(Comment.class, commentId);
        em.getTransaction().commit();
        return comment;
    }
}
