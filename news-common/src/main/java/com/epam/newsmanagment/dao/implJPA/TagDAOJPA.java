package com.epam.newsmanagment.dao.implJPA;

import com.epam.newsmanagment.dao.ITagDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.model.Tag;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Arseni_Vorhan on 9/17/2015.
 */
public class TagDAOJPA implements ITagDAO {

//    @PersistenceContext(unitName = "localPU")
//    private EntityManager em;
public EntityManager em = Persistence.createEntityManagerFactory("localPU").createEntityManager();

    public void link(Long tagId, Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public void unlink(Long tagId, Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public List<Tag> getAllTagsOfNews(Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public void unlinkAllTagsOfNews(Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public void linkTagsToNews(List<Long> tagIdList, Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public void unlinkTagFromAllNews(Long tagId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public List<Tag> getAllTags() throws DAOException {
        TypedQuery<Tag> namedQuery = em.createNamedQuery("Tag.getAll", Tag.class);
        return namedQuery.getResultList();
    }

    public Long addItem(Tag tag) throws DAOException {
        em.getTransaction().begin();
        em.persist(tag);
        em.getTransaction().commit();
        return tag.getTagId();
    }

    public void editItem(Tag tag) throws DAOException {
        em.getTransaction().begin();
        em.merge(tag);
        em.getTransaction().commit();
    }

    public void deleteItem(Long tagId) throws DAOException {
        em.getTransaction().begin();
        Tag tag = em.find(Tag.class, tagId);
        em.remove(tag);
        em.getTransaction().commit();
    }

    public Tag readItem(Long tagId) throws DAOException {
        Tag tag = null;
        em.getTransaction().begin();
        tag = em.find(Tag.class, tagId);
        em.getTransaction().commit();
        return tag;
    }
}
