package com.epam.newsmanagment.dao.implHibernate;

import com.epam.newsmanagment.dao.ICommentDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.model.Comment;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by Arseni_Vorhan on 9/17/2015.
 */
public class CommentDAOHibernate implements ICommentDAO {

    private SessionFactory sessionFactory;

    public CommentDAOHibernate(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public List<Comment> getAllCommentsForNews(Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public Long getCommentsCountForNews(Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public void deleteAllCommentsOfNews(Long newsId) throws DAOException {
        throw new UnsupportedOperationException();
    }

    public Long addItem(Comment comment) throws DAOException {
        Session session = null;
        try {
            session = getSession();
            session.save(comment);
        } catch (Exception e) {
            throw new DAOException(e);
        }
        return comment.getCommentId();
    }

    public void editItem(Comment entity) throws DAOException {

    }

    public void deleteItem(Long commentId) throws DAOException {
        Session session = null;
        try {
            session = getSession();
            Comment comment = (Comment)session.get(Comment.class, commentId);
            session.delete(comment);
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    public Comment readItem(Long itemId) throws DAOException {
        return null;
    }
}
