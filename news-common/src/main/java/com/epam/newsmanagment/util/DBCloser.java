package com.epam.newsmanagment.util;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBCloser {

	private static final Logger LOGGER = Logger.getLogger(DBCloser.class);

	public static void dataBaseCloser(DataSource dataSource, Connection connection,
			Statement statement, ResultSet resultSet) {

		if (connection != null && statement != null && resultSet != null) {
			try {
			DataSourceUtils.releaseConnection(connection, dataSource);
				statement.close();
				resultSet.close();
			} catch (SQLException e) {
				LOGGER.error("DBCloser exception", e);
			}

		}

	}

	public static void dataBaseCloser(DataSource dataSource, Connection connection,
			Statement statement) {

		if (connection != null && statement != null) {
			try {
				DataSourceUtils.releaseConnection(connection, dataSource);
				statement.close();
			} catch (SQLException e) {
				LOGGER.error("DBCloser exception", e);
			}

		}
	}

}
