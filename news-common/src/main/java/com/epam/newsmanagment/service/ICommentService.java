package com.epam.newsmanagment.service;

import com.epam.newsmanagment.dao.ICommentDAO;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.Comment;

import java.util.List;

/**
 * Created by Arseni on 7/20/2015.
 */
public interface ICommentService {
    public void setCommentDAO(ICommentDAO commentDAO);

    public Long createComment(Comment comment) throws ServiceException;
    public Comment getComment(Long commentId) throws ServiceException;
    public void updateComment(Comment comment) throws ServiceException;
    public void deleteComment(Long commentId) throws ServiceException;

    public List<Comment> getAllCommentsForNews(Long newsId) throws ServiceException;
    public Long getCommentsCountForNews(Long newsId) throws ServiceException;
    public void deleteAllCommentsOfNews(Long newsId) throws ServiceException;

}
