package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.ICommentDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.Comment;
import com.epam.newsmanagment.service.ICommentService;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Arseni on 7/20/2015.
 */
@Transactional(rollbackFor=Exception.class)
public class CommentService implements ICommentService {

    private static final Logger logger = Logger.getLogger(CommentService.class);

    ICommentDAO commentDAO;

    public void setCommentDAO(ICommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }

    public Long createComment(Comment comment) throws ServiceException {
        try {
            return commentDAO.addItem(comment);
        } catch (DAOException e) {
            logger.error("can't create comment");
            throw new ServiceException(e);
        }
    }

    public Comment getComment(Long commentId) throws ServiceException {
        try {
           return commentDAO.readItem(commentId);
        } catch (DAOException e) {
            logger.error("can't read comment");
            throw new ServiceException(e);
        }
    }

    public void updateComment(Comment comment) throws ServiceException {
        try {
            commentDAO.editItem(comment);
        } catch (DAOException e) {
            logger.error("can't update comment");
            throw new ServiceException(e);
        }
    }

    public void deleteComment(Long commentId) throws ServiceException {

        try {
            commentDAO.deleteItem(commentId);
        } catch (DAOException e) {
            logger.error("can't delete comment");
            throw new ServiceException(e);
        }
    }

    public List<Comment> getAllCommentsForNews(Long newsId) throws ServiceException {
        try {
           return commentDAO.getAllCommentsForNews(newsId);
        } catch (DAOException e) {
            logger.error("can't get comments");
            throw new ServiceException(e);
        }
    }

    public Long getCommentsCountForNews(Long newsId) throws ServiceException {
        try {
            return commentDAO.getCommentsCountForNews(newsId);
        } catch (DAOException e) {
            logger.error("can't count comments");
            throw new ServiceException(e);
        }
    }

    public void deleteAllCommentsOfNews(Long newsId) throws ServiceException {
        try {
            commentDAO.deleteAllCommentsOfNews(newsId);
        } catch (DAOException e) {
            logger.error("can't delete comments");
            throw new ServiceException(e);
        }
    }
}
