package com.epam.newsmanagment.service.implEclipseLink;

import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.*;
import com.epam.newsmanagment.service.*;
import com.epam.newsmanagment.service.implHibernate.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.*;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.swing.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

public class NewsManagmentServiceEclipseLink implements INewsManagementService {

    public EntityManager em = Persistence.createEntityManagerFactory("NEWS_MANAGEMENT").createEntityManager();


    public void setAuthorService(IAuthorService authorService) {

    }

    public void setCommentService(ICommentService commentService) {

    }

    public void setNewsService(INewsService newsService) {

    }

    public void setTagService(ITagService tagService) {

    }

    public Long addNews(NewsManagementVO valueObject) throws ServiceException {
        Session session = null;

        ArrayList<Tag> tagList = new ArrayList<Tag>();
        for(Long tagId : valueObject.getTagIdList()){
            tagList.add(new Tag(tagId));
        }
        valueObject.setTagList(tagList);

        Date date = new Date();
        java.sql.Date modificationDate = new java.sql.Date(date.getTime());
        valueObject.getNewsItem().setModificationDate(modificationDate);



        em.getTransaction().begin();

        News news = valueObject.getNewsItem();
        em.persist(news);
        valueObject.setNewsId(news.getNewsId());
        em.merge(valueObject);
        em.getTransaction().commit();


        return valueObject.getNewsId();
    }

    public void editNews(NewsManagementVO valueObject) throws ServiceException {
        Session session = null;

        ArrayList<Tag> tagList = new ArrayList<Tag>();
        for(Long tagId : valueObject.getTagIdList()){
            tagList.add(new Tag(tagId));
        }
        valueObject.setTagList(tagList);

        Date date = new Date();
        java.sql.Date modificationDate = new java.sql.Date(date.getTime());
        valueObject.getNewsItem().setModificationDate(modificationDate);



        em.getTransaction().begin();


        valueObject.setNewsId(valueObject.getNewsItem().getNewsId());

        em.merge(valueObject);
        em.getTransaction().commit();
    }

    public void deleteNews(Long newsId) throws ServiceException {

        em.getTransaction().begin();

        NewsManagementVO vo = em.find(NewsManagementVO.class, newsId);
        em.remove(vo);
        em.getTransaction().commit();
    }

    public NewsManagementVO getSingleNews(Long newsId) throws ServiceException {
        em.getTransaction().begin();

        NewsManagementVO vo = em.find(NewsManagementVO.class, newsId);
        em.getTransaction().commit();

        return vo;
    }

    public static void main(String[] args) {
        System.out.println("olololo");
    }

    public List<NewsManagementVO> searchNews(SearchCriteria searchCriteria) throws ServiceException {
        Session session = null;
        List<NewsManagementVO> newsManagementVOList = new ArrayList<NewsManagementVO>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();

            Criteria criteria = session.createCriteria(NewsManagementVO.class, "newsManagementVO");

            if(null != searchCriteria) {

                if (null != searchCriteria.getAuthorId()) {
                    criteria.add(Restrictions.eq("authorItem.authorId", searchCriteria.getAuthorId()));
                }

                if (null != searchCriteria.getTagIDs()) {
                    for (Long tagId : searchCriteria.getTagIDs()) {
                        DetachedCriteria subquery = DetachedCriteria.forClass(Tag.class, "tag");
                        subquery.add(Restrictions.eq("tagId", tagId));

                        subquery.setProjection(Projections.property("tagId"));
                        subquery.createAlias("newsList", "news");
                        subquery.add(Restrictions.eqProperty("news.newsId", "newsManagementVO.newsItem.newsId"));
                        criteria.add(Subqueries.exists(subquery));
                    }
                }
                criteria.createAlias("newsItem", "n");
                criteria.addOrder(Order.asc("n.modificationDate"));

                criteria.addOrder(Order.desc("commentsCount"));


                criteria.setFirstResult(searchCriteria.getMinCount().intValue()-1);
                criteria.setMaxResults(searchCriteria.getMaxCount().intValue() - searchCriteria.getMinCount().intValue()+1);


            }

            newsManagementVOList = criteria.list();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "?????? I/O", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return newsManagementVOList;
    }

    public Long addComment(String commentText, Long newsId) throws ServiceException {
        Comment comment = new Comment();
        comment.setCommentText(commentText);
        comment.setNewsId(newsId);

        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        comment.setCreationDate(timestamp);



        em.getTransaction().begin();

        em.persist(comment);
        em.getTransaction().commit();

        return comment.getCommentId();
    }

    public Long countAllNews() throws ServiceException {
        Session session = null;
        List<NewsManagementVO> newsManagementVOList = new ArrayList<NewsManagementVO>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            newsManagementVOList = session.createCriteria(NewsManagementVO.class).list();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "?????? I/O", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return Long.valueOf(newsManagementVOList.size());
    }

    public List<Author> getAllAuthors() throws ServiceException {
        Session session = null;
        List<Author> authors = new ArrayList<Author>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            authors = session.createCriteria(Author.class).list();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "?????? I/O", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return authors;
    }

    public Long countAllNews(SearchCriteria searchCriteria) throws ServiceException {
        List newsList = searchNews(searchCriteria);
        return Long.valueOf(newsList.size());
    }

    public List<Tag> getAllTags() throws ServiceException {
        Session session = null;
        List<Tag> authors = new ArrayList<Tag>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            authors = session.createCriteria(Tag.class).list();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "?????? I/O", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return authors;
    }

    public void deleteComment(Long commentId) throws ServiceException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Comment comment = (Comment)session.load(Comment.class, commentId);
            session.delete(comment);

            session.getTransaction().commit();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "������ I/O", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public void addAuthor(Author author) throws ServiceException {

        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(author);
            session.getTransaction().commit();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "?????? I/O", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public void updateAuthor(Author author) throws ServiceException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(author);
            session.getTransaction().commit();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "?????? I/O", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public void updateTag(Tag tag) throws ServiceException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(tag);
            session.getTransaction().commit();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "?????? I/O", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public void deleteTag(Long tagId) throws ServiceException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Tag tag = (Tag)session.load(Tag.class, tagId);
            session.delete(tag);

            session.getTransaction().commit();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "������ I/O", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public void addTag(Tag tag) throws ServiceException {

        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(tag);
            session.getTransaction().commit();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "?????? I/O", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public void expireAuthor(Long authorId) throws ServiceException {

    }

    public void deleteNewsList(List<Long> newsIdList) throws ServiceException {

        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            for(Long newsId : newsIdList) {
                News news = (News) session.load(News.class, newsId);
                session.delete(news);
            }

            session.getTransaction().commit();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "������ I/O", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

    }

    public Long getRownumByNewsId(SearchCriteria searchCriteria, Long newsId) throws ServiceException {

        searchCriteria.setMinCount(0L);
        searchCriteria.setMaxCount(countAllNews());
        List<NewsManagementVO> newsManagementVOList = searchNews(searchCriteria);

        for (int i = 0; i < newsManagementVOList.size(); i++){
            if(newsManagementVOList.get(i).getNewsItem().getNewsId().equals(newsId)){
                return Long.valueOf(i + 1);
            }
        }
        return null;
    }

    public Long getNextNewsId(SearchCriteria searchCriteria, Long newsId) throws ServiceException {
        searchCriteria.setMinCount(0L);
        searchCriteria.setMaxCount(countAllNews());
        List<NewsManagementVO> newsManagementVOList = searchNews(searchCriteria);

        ListIterator it = newsManagementVOList.listIterator();

        while(it.hasNext()){
            NewsManagementVO newsManagementVO = (NewsManagementVO) it.next();
            if(newsManagementVO.getNewsItem().getNewsId().equals(newsId)){
                NewsManagementVO nextNewsManagementVO = (NewsManagementVO) it.next();
                return nextNewsManagementVO.getNewsItem().getNewsId();
            }
        }
        return null;
    }

    public Long getPreviousNewsId(SearchCriteria searchCriteria, Long newsId) throws ServiceException {
        searchCriteria.setMinCount(0L);
        searchCriteria.setMaxCount(countAllNews());
        List<NewsManagementVO> newsManagementVOList = searchNews(searchCriteria);

        ListIterator it = newsManagementVOList.listIterator();

        while(it.hasNext()){
            NewsManagementVO newsManagementVO = (NewsManagementVO) it.next();
            if(newsManagementVO.getNewsItem().getNewsId().equals(newsId)){
                it.previous();
                NewsManagementVO prevNewsManagementVO = (NewsManagementVO) it.previous();
                return prevNewsManagementVO.getNewsItem().getNewsId();
            }
        }
        return null;
    }
}
