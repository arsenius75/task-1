package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.ITagDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.Tag;
import com.epam.newsmanagment.service.ITagService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Arseni on 7/21/2015.
 */
@Transactional(rollbackFor=Exception.class)
public class TagService implements ITagService {

    private static final Logger logger = Logger.getLogger(TagService.class);

    @Autowired
    ITagDAO tagDAO;

    public void setTagDAO(ITagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }

    public Long createTag(Tag tag) throws ServiceException {
        try {
            return tagDAO.addItem(tag);
        } catch (DAOException e) {
            logger.error("can't create tag");
            throw new ServiceException(e);
        }
    }

    public Tag getTag(Long tagId) throws ServiceException {
        try {
           return tagDAO.readItem(tagId);
        } catch (DAOException e) {
            logger.error("can't read tag");
            throw new ServiceException(e);
        }
    }

    public void updateTag(Tag tag) throws ServiceException {
        try {
            tagDAO.editItem(tag);
        } catch (DAOException e) {
            logger.error("can't edit tag");
            throw new ServiceException(e);
        }
    }

    public void deleteTag(Long tagId) throws ServiceException {
        try {
            tagDAO.deleteItem(tagId);
        } catch (DAOException e) {
            logger.error("can't delete tag");
            throw new ServiceException(e);
        }
    }

    public List<Tag> getAllTagsOfNews(Long newsId) throws ServiceException {
        try {
            return tagDAO.getAllTagsOfNews(newsId);
        } catch (DAOException e) {
            logger.error("can't get all tags of news");
            throw new ServiceException(e);
        }
    }

    public void linkTagToNews(Long tagId, Long newsId) throws ServiceException {
        try {
            tagDAO.link(tagId, newsId);
        } catch (DAOException e) {
            logger.error("can't link tag");
            throw new ServiceException(e);
        }
    }

    public void unlinkTagToNews(Long tagId, Long newsId) throws ServiceException {
        try {
            tagDAO.unlink(tagId, newsId);
        } catch (DAOException e) {
            logger.error("can't unlink tag");
            throw new ServiceException(e);
        }
    }

    public void unlinkAllTagsFromNews(Long newsId) throws ServiceException {
        try {
            tagDAO.unlinkAllTagsOfNews(newsId);
        } catch (DAOException e) {
            logger.error("can't unlink all tags");
            throw new ServiceException(e);
        }
    }

    public void linkTagsToNews(List<Long> tagIdList, Long newsId) throws ServiceException {
        try {
            tagDAO.linkTagsToNews(tagIdList, newsId);
        } catch (DAOException e) {
            logger.error("can't link tags");
            throw new ServiceException(e);
        }
    }

    public List<Tag> getAllTags() throws ServiceException {
        try {
            return tagDAO.getAllTags();
        } catch (DAOException e) {
            logger.error("can't get tags");
            throw new ServiceException(e);
        }
    }

    public void unlinkTagFromAllNews(Long tagId) throws ServiceException {
        try {
            tagDAO.unlinkTagFromAllNews(tagId);
        } catch (DAOException e) {
            logger.error("can't unlink tag");
            throw new ServiceException(e);
        }
    }
}
