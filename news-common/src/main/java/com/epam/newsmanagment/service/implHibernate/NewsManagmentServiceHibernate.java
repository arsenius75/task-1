package com.epam.newsmanagment.service.implHibernate;

import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.*;
import com.epam.newsmanagment.service.*;

import java.sql.Timestamp;
import java.util.*;

//@Transactional(rollbackFor = Exception.class)
public class NewsManagmentServiceHibernate implements INewsManagementService {
//    @Autowired
    private IAuthorService authorService;
//    @Autowired
    private INewsService newsService;
//    @Autowired
    private ICommentService commentService;
//    @Autowired
    private ITagService tagService;
//    @Autowired
//    private SessionFactory sessionFactory;

    public void setAuthorService(IAuthorService authorService) {
        this.authorService = authorService;
    }

    public void setCommentService(ICommentService commentService) {
        this.commentService = commentService;
    }

    public void setNewsService(INewsService newsService) {
        this.newsService = newsService;
    }

    public void setTagService(ITagService tagService) {
        this.tagService = tagService;
    }

    public Long addNews(NewsManagementVO valueObject) throws ServiceException {


        News news = valueObject.getNewsItem();

        Set<Tag> tagList = new HashSet<Tag>();
        for(Long tagId : valueObject.getTagIdList()){
            tagList.add(new Tag(tagId)
//                    tagService.getTag(tagId)
            );
        }
        news.setTagList(tagList);

        Date date = new Date();
        java.sql.Date modificationDate = new java.sql.Date(date.getTime());
        news.setModificationDate(modificationDate);

        news.setAuthorItem(valueObject.getAuthorItem());
        news.setCommentList(new HashSet<Comment>(valueObject.getCommentList()));

        return newsService.createNews(news);
    }

    public void editNews(NewsManagementVO valueObject) throws ServiceException {

        News news = valueObject.getNewsItem();

        Set<Tag> tagList = new HashSet<Tag>();
        for(Long tagId : valueObject.getTagIdList()){
            tagList.add(tagService.getTag(tagId)
//                    new Tag(tagId)
            );
        }
        news.setTagList(tagList);

        Date date = new Date();
        java.sql.Date modificationDate = new java.sql.Date(date.getTime());
        news.setModificationDate(modificationDate);

        news.setAuthorItem(valueObject.getAuthorItem());
        news.setCommentList(new HashSet<Comment>(valueObject.getCommentList()));

        newsService.updateNews(news);
    }

    public void deleteNews(Long newsId) throws ServiceException {
        newsService.deleteNews(newsId);
    }

    public NewsManagementVO getSingleNews(Long newsId) throws ServiceException {


        News news = newsService.getNews(newsId);

        NewsManagementVO newsManagementVO = new NewsManagementVO();

        newsManagementVO.setNewsItem(news);
        newsManagementVO.setAuthorItem(news.getAuthorItem());
        newsManagementVO.setCommentList(new ArrayList<Comment>(news.getCommentList()));
        newsManagementVO.setTagList(new ArrayList<Tag>(news.getTagList()));
        return newsManagementVO;
    }

    public List<NewsManagementVO> searchNews(SearchCriteria searchCriteria) throws ServiceException {

        List<News> newsList = newsService.searchNews(searchCriteria);

        List<NewsManagementVO> newsManagementVOList = new ArrayList<NewsManagementVO>();

        for(News news : newsList){
            NewsManagementVO vo = new NewsManagementVO();
            vo.setNewsItem(news);
            vo.setAuthorItem(news.getAuthorItem());
            vo.setCommentList(new ArrayList<Comment>(news.getCommentList()));
            vo.setTagList(new ArrayList<Tag>(news.getTagList()));

            newsManagementVOList.add(vo);
        }
        return newsManagementVOList;
    }

    public Long addComment(String commentText, Long newsId) throws ServiceException {
        Comment comment = new Comment();
        comment.setCommentText(commentText);
        comment.setNewsId(newsId);

        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        comment.setCreationDate(timestamp);


        return commentService.createComment(comment);

    }

    public Long countAllNews() throws ServiceException {
        return newsService.countAllNews();
    }

    public List<Author> getAllAuthors() throws ServiceException {
        return authorService.getAllAuthors();
    }

    public Long countAllNews(SearchCriteria searchCriteria) throws ServiceException {
        return newsService.countAllNews(searchCriteria);
    }

    public List<Tag> getAllTags() throws ServiceException {
        return tagService.getAllTags();
    }

    public void deleteComment(Long commentId) throws ServiceException {
        commentService.deleteComment(commentId);

    }

    public void addAuthor(Author author) throws ServiceException {
        authorService.createAuthor(author);
    }

    public void updateAuthor(Author author) throws ServiceException {
        authorService.updateAuthor(author);
    }

    public void updateTag(Tag tag) throws ServiceException {
        tagService.updateTag(tag);
    }

    public void deleteTag(Long tagId) throws ServiceException {
        tagService.deleteTag(tagId);
    }

    public void addTag(Tag tag) throws ServiceException {
        tagService.createTag(tag);
    }

    public void expireAuthor(Long authorId) throws ServiceException {
        authorService.expireAuthor(authorId);
    }

    public void deleteNewsList(List<Long> newsIdList) throws ServiceException {

       newsService.deleteNewsList(newsIdList);

    }

    public Long getRownumByNewsId(SearchCriteria searchCriteria, Long newsId) throws ServiceException {
        return newsService.getRownumByNewsId(searchCriteria, newsId);
    }

}