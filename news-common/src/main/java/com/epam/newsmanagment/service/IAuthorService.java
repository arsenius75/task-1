package com.epam.newsmanagment.service;

import com.epam.newsmanagment.dao.IAuthorDAO;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.Author;

import java.util.List;

/**
 * Created by Arseni on 7/20/2015.
 */
public interface IAuthorService {
    public void setAuthorDAO(IAuthorDAO authorDAO);

    public Long createAuthor(Author author) throws ServiceException;
    public void deleteAuthor(Long authorId) throws ServiceException;
    public void updateAuthor(Author author) throws ServiceException;
    public Author getAuthor(Long authorId) throws ServiceException;

    public void linkAuthorToNews(Long authorId, Long newsId) throws ServiceException;
    public void unlinkAuthorFromNews(Long newsId) throws ServiceException;

    public Author getAuthorOfNews(Long newsId) throws ServiceException;
    public List<Author> getAllAuthors() throws ServiceException;

    void expireAuthor(Long authorId) throws ServiceException;
}
