package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.INewsDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.News;
import com.epam.newsmanagment.model.SearchCriteria;
import com.epam.newsmanagment.service.INewsService;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Arseni on 7/21/2015.
 */
@Transactional(rollbackFor=Exception.class)
public class NewsService implements INewsService {

    private static final Logger logger = Logger.getLogger(NewsService.class);

    INewsDAO newsDAO;

    public void setNewsDAO(INewsDAO newsDAO) {
        this.newsDAO = newsDAO;
    }

    public Long createNews(News news) throws ServiceException {
        try {
            return newsDAO.addItem(news);
        } catch (DAOException e) {
            logger.error("can't create news");
            throw new ServiceException(e);
        }
    }

    public News getNews(Long newsId) throws ServiceException {
        try {
            return newsDAO.readItem(newsId);
        } catch (DAOException e) {
            logger.error("can't read news");
            throw new ServiceException(e);
        }
    }

    public void updateNews(News news) throws ServiceException {
        try {
            newsDAO.editItem(news);
        } catch (DAOException e) {
            logger.error("can't update news");
            throw new ServiceException(e);
        }
    }

    public void deleteNews(Long newsId) throws ServiceException {
        try {
            newsDAO.deleteItem(newsId);
        } catch (DAOException e) {
            logger.error("can't delete news");
            throw new ServiceException(e);
        }
    }

    public Long countAllNews() throws ServiceException {
        try {
            return newsDAO.countAllNews();
        } catch (DAOException e) {
            logger.error("can't count all news");
            throw new ServiceException(e);
        }
    }

    public List<News> searchNews(SearchCriteria searchCriteria) throws ServiceException {
        try {
            return newsDAO.searchNews(searchCriteria);
        } catch (DAOException e) {
            logger.error("search operation was fail");
            throw new ServiceException(e);
        }
    }

    public Long countAllNews(SearchCriteria searchCriteria) throws ServiceException {
        try {
            return newsDAO.countAllNews(searchCriteria);
        } catch (DAOException e) {
            logger.error("can't get news count");
            throw new ServiceException(e);
        }
    }

    public void deleteNewsList(List<Long> newsIdList) throws ServiceException {
        try {
            newsDAO.deleteNewsList(newsIdList);
        } catch (DAOException e) {
            logger.error("can't delete newslist");
            throw new ServiceException(e);
        }
    }

    public Long getRownumByNewsId(SearchCriteria searchCriteria, Long newsId) throws ServiceException {
        try {
            return newsDAO.getRownumByNewsId(searchCriteria, newsId);
        } catch (DAOException e) {
            logger.error("can't get rownum by id");
            throw new ServiceException(e);
        }
    }
}