package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.IAuthorDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.Author;
import com.epam.newsmanagment.service.IAuthorService;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Arseni on 7/20/2015.
 */
@Transactional(rollbackFor=Exception.class)
public class AuthorService implements IAuthorService {

    private static final Logger logger = Logger.getLogger(AuthorService.class);

    IAuthorDAO authorDAO;



    public void setAuthorDAO(IAuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }

    public Long createAuthor(Author author) throws ServiceException {
        try {
            return authorDAO.addItem(author);
        } catch (DAOException e) {
            logger.error("can't create author");
            throw new ServiceException(e);
        }
    }

    public void deleteAuthor(Long authorId) throws ServiceException {
        try {
            authorDAO.deleteItem(authorId);
        } catch (DAOException e) {
            logger.error("can't delete author");
            throw new ServiceException(e);
        }
    }

    public void updateAuthor(Author author) throws ServiceException {
        try {
            authorDAO.editItem(author);
        } catch (DAOException e) {
            logger.error("can't edit author");
            throw new ServiceException(e);
        }
    }

    public Author getAuthor(Long authorId) throws ServiceException {
        try {
           return authorDAO.readItem(authorId);
        } catch (DAOException e) {
            logger.error("can't read author");
            throw new ServiceException(e);
        }
    }

    public void linkAuthorToNews(Long authorId, Long newsId) throws ServiceException {
        try {
            authorDAO.link(authorId, newsId);
        } catch (DAOException e) {
            logger.error("can't link author");
            throw new ServiceException(e);
        }
    }

    public void unlinkAuthorFromNews(Long newsId) throws ServiceException {
        try {
            authorDAO.unlink(newsId);
        } catch (DAOException e) {
            logger.error("can't unlink author");
            throw new ServiceException(e);
        }
    }

    public Author getAuthorOfNews(Long newsId) throws ServiceException {
        try {
            return authorDAO.getAuthorOfNews(newsId);
        } catch (DAOException e) {
            logger.error("can't find author of news");
            throw new ServiceException(e);
        }
    }

    public List<Author> getAllAuthors() throws ServiceException {
        try {
            return authorDAO.getAllAuthors();
        } catch (DAOException e) {
            logger.error("can't get authors");
            throw new ServiceException(e);
        }
    }

    public void expireAuthor(Long authorId) throws ServiceException {
        try {
            authorDAO.expireAuthor(authorId);
        } catch (DAOException e) {
            logger.error("can't expire author");
            throw new ServiceException(e);
        }
    }
}
