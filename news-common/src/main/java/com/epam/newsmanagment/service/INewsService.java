package com.epam.newsmanagment.service;

import com.epam.newsmanagment.dao.INewsDAO;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.News;
import com.epam.newsmanagment.model.SearchCriteria;

import java.util.List;

/**
 * Created by Arseni on 7/20/2015.
 */
public interface INewsService {
    public void setNewsDAO(INewsDAO newsDAO);

    public Long createNews(News news) throws ServiceException;
    public News getNews(Long newsId) throws ServiceException;
    public void updateNews(News news) throws ServiceException;
    public void deleteNews(Long newsId) throws ServiceException;

    Long countAllNews() throws ServiceException;
    public List<News> searchNews(SearchCriteria searchCriteria) throws ServiceException;

    Long countAllNews(SearchCriteria searchCriteria) throws ServiceException;

    void deleteNewsList(List<Long> newsIdList) throws ServiceException;

    Long getRownumByNewsId(SearchCriteria searchCriteria, Long newsId) throws ServiceException;
}
