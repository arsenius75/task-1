package com.epam.newsmanagment.service;

import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.Author;
import com.epam.newsmanagment.model.NewsManagementVO;
import com.epam.newsmanagment.model.SearchCriteria;
import com.epam.newsmanagment.model.Tag;

import java.util.List;

/**
 * Created by Arseni on 7/20/2015.
 */
public interface INewsManagementService {
    public Long addNews(NewsManagementVO valueObject) throws ServiceException;

    public void editNews(NewsManagementVO valueObject) throws ServiceException;

    public void deleteNews(Long newsId) throws ServiceException;

    //    public List<NewsManagementVO> getNewsList() throws ServiceException;
    public NewsManagementVO getSingleNews(Long newsId) throws ServiceException;

    //    public void addNewsAuthor(Long authorId, Long newsId) throws ServiceException;
    public List<NewsManagementVO> searchNews(SearchCriteria searchCriteria) throws ServiceException;

    //    public void linkNewsTags(List<Long> tagId, Long newsId) throws ServiceException;
    public Long addComment(String commentText, Long newsId) throws ServiceException;

    //    public void deleteComments(Long newsId) throws ServiceException;
    public Long countAllNews() throws ServiceException;

    public List<Author> getAllAuthors() throws ServiceException;

    Long countAllNews(SearchCriteria searchCriteria) throws ServiceException;

    public void setAuthorService(IAuthorService authorService);

    public void setCommentService(ICommentService commentService);

    public void setNewsService(INewsService newsService);

    public void setTagService(ITagService tagService);

    List<Tag> getAllTags() throws ServiceException;

    void deleteComment(Long commentId) throws ServiceException;

    void addAuthor(Author author) throws ServiceException;

    void updateAuthor(Author author) throws ServiceException;

    void updateTag(Tag tag) throws ServiceException;

    void deleteTag(Long tagId) throws ServiceException;

    void addTag(Tag tag) throws ServiceException;

    void expireAuthor(Long authorId) throws ServiceException;

    void deleteNewsList(List<Long> newsIdList) throws ServiceException;

    Long getRownumByNewsId(SearchCriteria searchCriteria, Long newsId) throws ServiceException;


}
