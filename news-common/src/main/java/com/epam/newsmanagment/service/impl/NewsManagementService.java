package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.*;
import com.epam.newsmanagment.service.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Arseni on 7/21/2015.
 */
@Transactional(rollbackFor=Exception.class)
public class NewsManagementService implements INewsManagementService {

    private IAuthorService authorService;
    private INewsService newsService;
    private ICommentService commentService;
    private ITagService tagService;

    public void setAuthorService(IAuthorService authorService) {
        this.authorService = authorService;
    }

    public void setNewsService(INewsService newsService) {
        this.newsService = newsService;
    }

    public void setCommentService(ICommentService commentService) {
        this.commentService = commentService;
    }

    public void setTagService(ITagService tagService) {
        this.tagService = tagService;
    }

    public List<Tag> getAllTags() throws ServiceException {
        return tagService.getAllTags();
    }

    /**
     * this method gets news, list of tags and author from ValueObject and adds al this
     * @param valueObject
     * @throws ServiceException
     */
    public Long addNews(NewsManagementVO valueObject) throws ServiceException {

        Author author = valueObject.getAuthorItem();
        News news = valueObject.getNewsItem();
        List<Long> tagIdList= valueObject.getTagIdList();

        Long newsId = newsService.createNews(news);
        authorService.linkAuthorToNews(author.getAuthorId(), newsId);

        for(Long tagId : tagIdList){
            tagService.linkTagToNews(tagId, newsId);
        }
//        tagService.linkTagsToNews(tagIdList, newsId);

        return newsId;
    }

    /**
     * edit news
     * @param valueObject
     * @throws ServiceException
     */
    public void editNews(NewsManagementVO valueObject) throws ServiceException {
        Author author = valueObject.getAuthorItem();
        News news = valueObject.getNewsItem();
//        List<Tag> tagList= valueObject.getTagList();


        newsService.updateNews(news);

        Long newsId = news.getNewsId();


            authorService.unlinkAuthorFromNews(newsId);
            authorService.linkAuthorToNews(author.getAuthorId(), newsId);


            tagService.unlinkAllTagsFromNews(newsId);

        for(Long tagId : valueObject.getTagIdList()){
            tagService.linkTagToNews(tagId, newsId);
        }
    }

    /**
     * delete news
     * @param newsId
     * @throws ServiceException
     */
    public void deleteNews(Long newsId) throws ServiceException {
        tagService.unlinkAllTagsFromNews(newsId);
        authorService.unlinkAuthorFromNews(newsId);
        commentService.deleteAllCommentsOfNews(newsId);
        newsService.deleteNews(newsId);
    }

    /**
     * gets single news Value Object by ID
     * @param newsId
     * @return
     * @throws ServiceException
     */
    public NewsManagementVO getSingleNews(Long newsId) throws ServiceException {

        News news = newsService.getNews(newsId);
        Author author = authorService.getAuthorOfNews(newsId);
        List<Tag> tagList = tagService.getAllTagsOfNews(newsId);
        List<Comment> commentList = commentService.getAllCommentsForNews(newsId);

        NewsManagementVO vo = new NewsManagementVO();
        vo.setNewsItem(news);
        vo.setAuthorItem(author);
        vo.setTagList(tagList);
        vo.setCommentList(commentList);

        return vo;
    }

    /**
     * search news by search criteria
     * @param searchCriteria contains id of author and list of tag's id
     * @return
     * @throws ServiceException
     */
    public List<NewsManagementVO> searchNews(SearchCriteria searchCriteria) throws ServiceException {

        List<NewsManagementVO> newsList = new ArrayList<NewsManagementVO>();

        List<News> allNews = newsService.searchNews(searchCriteria);

        for(News news : allNews){
            Long newsId = news.getNewsId();
            Author author = authorService.getAuthorOfNews(newsId);
            List<Tag> tagList = tagService.getAllTagsOfNews(newsId);
            List<Comment> commentList = commentService.getAllCommentsForNews(newsId);

            NewsManagementVO vo = new NewsManagementVO();
            vo.setNewsItem(news);
            vo.setAuthorItem(author);
            vo.setTagList(tagList);
            vo.setCommentList(commentList);

            newsList.add(vo);
        }

        return newsList;
    }

    /**
     * add comment to news
     * @param commentText
     * @param newsId
     * @return
     * @throws ServiceException
     */
    public Long addComment(String commentText, Long newsId) throws ServiceException {
        Comment comment = new Comment();
        comment.setCommentText(commentText);
        comment.setNewsId(newsId);

        return commentService.createComment(comment);
    }

    /**
     *
     * @return  number of all news
     * @throws ServiceException
     */
    public Long countAllNews() throws ServiceException {
        return newsService.countAllNews();
    }

    /**
     *
     * @return list of all authors
     * @throws ServiceException
     */
    public List<Author> getAllAuthors() throws ServiceException {
        return authorService.getAllAuthors();
    }

    public Long countAllNews(SearchCriteria searchCriteria) throws ServiceException {
        return newsService.countAllNews(searchCriteria);
    }


    public void deleteComment(Long commentId) throws ServiceException {
        commentService.deleteComment(commentId);
    }

    public void addAuthor(Author author) throws ServiceException {
        authorService.createAuthor(author);
    }

    public void updateAuthor(Author author) throws ServiceException {
        authorService.updateAuthor(author);
    }

    public void updateTag(Tag tag) throws ServiceException {
        tagService.updateTag(tag);
    }

    public void deleteTag(Long tagId) throws ServiceException {
        tagService.unlinkTagFromAllNews(tagId);
        tagService.deleteTag(tagId);
    }

    public void addTag(Tag tag) throws ServiceException {
        tagService.createTag(tag);
    }

    public void expireAuthor(Long authorId) throws ServiceException {
        authorService.expireAuthor(authorId);
    }

    public void deleteNewsList(List<Long> newsIdList) throws ServiceException {
        for(Long newsId : newsIdList) {
            authorService.unlinkAuthorFromNews(newsId);
            commentService.deleteAllCommentsOfNews(newsId);
            tagService.unlinkAllTagsFromNews(newsId);
            newsService.deleteNews(newsId);
        }
    }

    public Long getRownumByNewsId(SearchCriteria searchCriteria, Long newsId) throws ServiceException {

        searchCriteria.setMinCount(0L);
        searchCriteria.setMaxCount(newsService.countAllNews());
        return newsService.getRownumByNewsId(searchCriteria, newsId);
//        List<News> allNews = newsService.searchNews(searchCriteria);
//        for(News news : allNews){
//            if(news.getNewsId().equals(newsId)){
//                return news.getRownum();
//            }
//        }
//        return null;
    }

    public Long getNextNewsId(SearchCriteria searchCriteria, Long newsId) throws ServiceException {
        searchCriteria.setMinCount(0L);
        searchCriteria.setMaxCount(countAllNews());
        List<NewsManagementVO> newsManagementVOList = searchNews(searchCriteria);

        ListIterator it = newsManagementVOList.listIterator();

        while(it.hasNext()){
            NewsManagementVO newsManagementVO = (NewsManagementVO) it.next();
            if(newsManagementVO.getNewsItem().getNewsId().equals(newsId)){
                NewsManagementVO nextNewsManagementVO = (NewsManagementVO) it.next();
                return nextNewsManagementVO.getNewsItem().getNewsId();
            }
        }


//        for (NewsManagementVO vo : newsManagementVOList){
//            if(vo.getNewsItem().getNewsId().equals(newsId)){
//                return newsManagementVOList.get(i+1).getNewsItem().getNewsId();
//            }
//            i++;
//        }
        return null;
    }

    public Long getPreviousNewsId(SearchCriteria searchCriteria, Long newsId) throws ServiceException {
        searchCriteria.setMinCount(0L);
        searchCriteria.setMaxCount(countAllNews());
        List<NewsManagementVO> newsManagementVOList = searchNews(searchCriteria);

        ListIterator it = newsManagementVOList.listIterator();

        while(it.hasNext()){
            NewsManagementVO newsManagementVO = (NewsManagementVO) it.next();
            if(newsManagementVO.getNewsItem().getNewsId().equals(newsId)){
                NewsManagementVO prevNewsManagementVO = (NewsManagementVO) it.previous();
                return prevNewsManagementVO.getNewsItem().getNewsId();
            }
        }
        return null;
    }
}