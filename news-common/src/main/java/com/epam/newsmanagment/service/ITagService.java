package com.epam.newsmanagment.service;

import com.epam.newsmanagment.dao.ITagDAO;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.Tag;

import java.util.List;

/**
 * Created by Arseni on 7/20/2015.
 */
public interface ITagService {
    public void setTagDAO(ITagDAO tagDAO);

    public Long createTag(Tag tag) throws ServiceException;
    public Tag getTag(Long tagId) throws ServiceException;
    public void updateTag(Tag tag) throws ServiceException;
    public void deleteTag(Long tagId) throws ServiceException;

    public List<Tag> getAllTagsOfNews(Long newsId) throws ServiceException;
    public void linkTagToNews(Long tagId, Long newsId) throws ServiceException;

    public void unlinkTagToNews(Long tagId, Long newsId) throws ServiceException;
    public void unlinkAllTagsFromNews(Long newsId) throws ServiceException;

    public void linkTagsToNews(List<Long> tagIdList, Long newsId) throws ServiceException;

    List<Tag> getAllTags() throws ServiceException;

    void unlinkTagFromAllNews(Long tagId) throws ServiceException;
}
