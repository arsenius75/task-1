package com.epam.newsmanagment.service;

import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.User;

/**
 * Created by Arseni on 8/10/2015.
 */
public interface IUserService {

//    public void setUserDAO(ITagDAO tagDAO);

    public Long createUser(User user) throws ServiceException;

    public User getUser(Long userId) throws ServiceException;

//    public void updateTag(Tag tag) throws ServiceException;
//    public void deleteTag(Long tagId) throws ServiceException;

//    public List<Tag> getAllTagsOfNews(Long newsId) throws ServiceException;
//    public void linkTagToNews(Long tagId, Long newsId) throws ServiceException;
//
//    public void unlinkTagToNews(Long tagId, Long newsId) throws ServiceException;
//    public void unlinkAllTagsFromNews(Long newsId) throws ServiceException;
//
//    public void linkTagsToNews(List<Long> tagIdList, Long newsId) throws ServiceException;
//
//    List<Tag> getAllTags() throws ServiceException;
}

