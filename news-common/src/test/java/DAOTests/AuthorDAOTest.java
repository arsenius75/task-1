package DAOTests;

import com.epam.newsmanagment.model.Author;
import com.epam.newsmanagment.dao.IAuthorDAO;
import com.epam.newsmanagment.exception.DAOException;
import org.junit.Ignore;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBeanByName;

import javax.sql.DataSource;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.unitils.reflectionassert.ReflectionAssert.assertPropertyLenientEquals;

@DataSet("dataSet.xml")
@Transactional(TransactionMode.ROLLBACK)
@SpringApplicationContext("news-common-context.xml")
@Ignore
public class AuthorDAOTest extends UnitilsJUnit4 {


	@SpringBeanByName
//    @TestDataSource
	private DataSource dataSource;
	@SpringBeanByName
	private IAuthorDAO authorDAO;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void setAuthorDAO(IAuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}

    @Test
    public void getAllAuthors() throws DAOException {

        List<Author> authors = authorDAO.getAllAuthors();
        assertPropertyLenientEquals("authorName", "John", authors.get(0));
    }

    @Test
    public void readAuthor() throws DAOException {

        Author resultAuthor = null;
        resultAuthor = authorDAO.readItem(1L);
        assertPropertyLenientEquals("authorName", "John", resultAuthor);
    }

    @Test
    public void createAuthor() throws DAOException {

        Author author = new Author();
        author.setAuthorName("Ololoev");
        Long id = authorDAO.addItem(author);

        Author resultAuthor = null;
        resultAuthor = authorDAO.readItem(id);
        assertPropertyLenientEquals("authorName", author.getAuthorName(), resultAuthor);
    }

    @Test
    public void updateAuthor() throws DAOException {

        Author author = new Author();
        author.setAuthorName("Ololoev");
        author.setAuthorId(1L);

        authorDAO.editItem(author);

        Author resultAuthor = null;
        resultAuthor = authorDAO.readItem(1L);
        assertPropertyLenientEquals("authorName", author.getAuthorName(), resultAuthor);
    }

    @Test
    public void deleteAuthor() throws DAOException {

        authorDAO.deleteItem(5L);

        Author resultAuthor = authorDAO.readItem(5L);

        assertEquals(null, resultAuthor);
    }


    @Test
    public void getAuthorOfNews() throws DAOException {

        Author author = authorDAO.getAuthorOfNews(3L);

        assertPropertyLenientEquals("authorName", "Bill", author);
    }

    @Test
    public void linkAuthor() throws DAOException {


        authorDAO.link(5L, 5L);

        Author author = authorDAO.getAuthorOfNews(5L);

        assertPropertyLenientEquals("authorName", "Vlad", author);
    }

    @Test
    public void unlinkAuthor() throws DAOException {


        authorDAO.unlink(5L);

        Author author = authorDAO.getAuthorOfNews(5L);

        assertEquals(null, author);
    }

}