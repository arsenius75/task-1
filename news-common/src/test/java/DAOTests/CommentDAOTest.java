package DAOTests;

import com.epam.newsmanagment.model.Comment;
import com.epam.newsmanagment.dao.ICommentDAO;
import com.epam.newsmanagment.exception.DAOException;
import org.junit.Ignore;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBeanByName;

import javax.sql.DataSource;
import java.util.List;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.unitils.reflectionassert.ReflectionAssert.assertPropertyLenientEquals;

@DataSet("dataSet.xml")
@Transactional(TransactionMode.ROLLBACK)
@SpringApplicationContext("news-common-context.xml")
@Ignore
public class CommentDAOTest extends UnitilsJUnit4 {


    @SpringBeanByName
//    @TestDataSource
    private DataSource dataSource;
    @SpringBeanByName
    private ICommentDAO commentDAO;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setCommentDAO(ICommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }


    @Test
    public void readComment() throws DAOException {

        Comment resultComment = commentDAO.readItem(1L);
        assertPropertyLenientEquals("commentText", "text1", resultComment);
    }

    @Test
    public void createComment() throws DAOException {

        Comment comment = new Comment();
        comment.setCommentText("t");
        comment.setNewsId(1L);
        Long id = commentDAO.addItem(comment);

        Comment resultComment = commentDAO.readItem(id);

        assertPropertyLenientEquals("commentText", "t", resultComment);
    }

    @Test
    public void updateComment() throws DAOException {

        Comment comment = new Comment();
        comment.setCommentText("updated comment");
        comment.setCommentId(1L);

        commentDAO.editItem(comment);

        Comment resultComment = commentDAO.readItem(1L);

        assertPropertyLenientEquals("commentText", comment.getCommentText(), resultComment);
    }

    @Test
    public void deleteComment() throws DAOException {

        commentDAO.deleteItem(6L);

        Comment resultComment = commentDAO.readItem(6L);

        assertEquals(null, resultComment);
    }

    @Test
    public void getAllCommentsForNews() throws DAOException {

        List<Comment> comments = commentDAO.getAllCommentsForNews(4L);

        assertPropertyLenientEquals("commentText", "text7", comments.get(0));
    }

    @Test
    public void deleteAllCommentsOfNews() throws DAOException {

        commentDAO.deleteAllCommentsOfNews(4L);

        List<Comment> comments = commentDAO.getAllCommentsForNews(4L);

        assertTrue(comments.isEmpty());
    }

    @Test
    public void getCommentsCountForNews() throws DAOException {

        Long count = commentDAO.getCommentsCountForNews(2L);

        assertEquals(Long.valueOf(2), count);
    }

}