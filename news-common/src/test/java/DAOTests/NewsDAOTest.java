package DAOTests;

import com.epam.newsmanagment.dao.INewsDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.model.News;
import com.epam.newsmanagment.model.SearchCriteria;
import org.junit.Ignore;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBeanByName;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.unitils.reflectionassert.ReflectionAssert.assertPropertyLenientEquals;

@DataSet("dataSet.xml")
@Transactional(TransactionMode.ROLLBACK)
@SpringApplicationContext("news-common-context.xml")
@Ignore
public class NewsDAOTest extends UnitilsJUnit4 {


    @SpringBeanByName
//    @TestDataSource
    private DataSource dataSource;
    @SpringBeanByName
    private INewsDAO newsDAO;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @Test
    public void readNews() throws DAOException {

        News resultNews = null;
        resultNews = newsDAO.readItem(1L);
        assertPropertyLenientEquals("title", "title1", resultNews);
        assertPropertyLenientEquals("fullText", "full_text1", resultNews);
        assertPropertyLenientEquals("shortText", "short_text1", resultNews);
    }

    @Test
    public void createNews() throws DAOException {

        News news = new News();
        news.setFullText("ft");
        news.setShortText("st");
        news.setTitle("t");

        Calendar calendar = Calendar.getInstance();
        java.util.Date now = calendar.getTime();
        java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());

        news.setCreationDate(currentTimestamp);

        Long id = newsDAO.addItem(news);

        News resultNews = newsDAO.readItem(id);

        assertPropertyLenientEquals("title", news.getTitle(), resultNews);
        assertPropertyLenientEquals("fullText", news.getFullText(), resultNews);
        assertPropertyLenientEquals("shortText", news.getShortText(), resultNews);
    }

    @Test
    public void updateNews() throws DAOException {

        News news = new News();
        news.setFullText("ft");
        news.setShortText("st");
        news.setTitle("t");
        news.setNewsId(1L);

        Calendar calendar = Calendar.getInstance();
        java.util.Date now = calendar.getTime();
        java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());

        news.setCreationDate(currentTimestamp);
        newsDAO.editItem(news);
        News resultNews = newsDAO.readItem(1L);
        assertPropertyLenientEquals("title", news.getTitle(), resultNews);
        assertPropertyLenientEquals("fullText", news.getFullText(), resultNews);
        assertPropertyLenientEquals("shortText", news.getShortText(), resultNews);
    }

    @Test
    public void deleteNews() throws DAOException {

        newsDAO.deleteItem(10L);
        News resultNews = newsDAO.readItem(10L);

        assertEquals(null, resultNews);
    }


    @Test
    public void countAllNews() throws DAOException {

        Long count = newsDAO.countAllNews();

        assertEquals(Long.valueOf(9), count);
    }

    @Test
    public void searchNews() throws DAOException {


        SearchCriteria searchCriteria = new SearchCriteria();

        List<Long> tagList = new ArrayList<Long>();
        tagList.add(1L);
        tagList.add(2L);
        tagList.add(3L);

        searchCriteria.setTagIDs(tagList);
        searchCriteria.setAuthorId(1L);
        searchCriteria.setMinCount(1L);
        searchCriteria.setMaxCount(3L);

        List<News> foundNews = newsDAO.searchNews(searchCriteria);

        News news = foundNews.get(0);

        assertEquals(Long.valueOf(1), news.getNewsId());
    }

}