package DAOTests;

import com.epam.newsmanagment.model.Tag;
import com.epam.newsmanagment.dao.ITagDAO;
import com.epam.newsmanagment.exception.DAOException;
import org.junit.Ignore;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.Transactional;
import org.unitils.database.util.TransactionMode;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBeanByName;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.unitils.reflectionassert.ReflectionAssert.assertPropertyLenientEquals;

@DataSet("dataSet.xml")
@Transactional(TransactionMode.ROLLBACK)
@SpringApplicationContext("news-common-context.xml")
@Ignore
public class TagDAOTest extends UnitilsJUnit4 {


    @SpringBeanByName
//    @TestDataSource
    private DataSource dataSource;
    @SpringBeanByName
    private ITagDAO tagDAO;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setTagDAO(ITagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }



    @Test
    public void readTag() throws DAOException {

        Tag resultTag = tagDAO.readItem(1L);
        assertPropertyLenientEquals("tagName", "tag1", resultTag);
    }

    @Test
    public void createTag() throws DAOException {

        Tag tag = new Tag();
        tag.setTagName("t");
        Long id = tagDAO.addItem(tag);

        Tag resultTag = tagDAO.readItem(id);

        assertPropertyLenientEquals("tagName", tag.getTagName(), resultTag);
    }

    @Test
    public void updateTag() throws DAOException {

        Tag tag = new Tag();
        tag.setTagName("updated tag");
        tag.setTagId(1L);

        tagDAO.editItem(tag);

        Tag resultTag = tagDAO.readItem(1L);

        assertPropertyLenientEquals("tagName", tag.getTagName(), resultTag);
    }

    @Test
    public void deleteTag() throws DAOException {

        tagDAO.deleteItem(9L);

        Tag resultTag = tagDAO.readItem(9L);

        assertEquals(null, resultTag);
    }


    @Test
    public void linkTag() throws DAOException {


        tagDAO.link(5L, 5L);

        List<Tag> tags = tagDAO.getAllTagsOfNews(5L);

        Tag tag = tags.get(0);

        assertPropertyLenientEquals("tagName", "tag5", tag);
    }

    @Test
    public void unlinkTag() throws DAOException {

        tagDAO.unlink(5L, 5L);

        List<Tag> tags =  tagDAO.getAllTagsOfNews(5L);

        assertTrue(tags.isEmpty());
    }



    @Test
    public void linkTags() throws DAOException {


        List<Long> tagIdList = new ArrayList<Long>();
        tagIdList.add(1L);
        tagIdList.add(2L);
        tagIdList.add(3L);

        tagDAO.linkTagsToNews(tagIdList, 5L);

        List<Tag> tags = tagDAO.getAllTagsOfNews(5L);

        Tag tag = tags.get(0);

        assertPropertyLenientEquals("tagName", "tag3", tag);
    }




    @Test
    public void unlinkAllTags() throws DAOException {

        tagDAO.unlinkAllTagsOfNews(3L);

        List<Tag> tags =  tagDAO.getAllTagsOfNews(3L);

        assertTrue(tags.isEmpty());
    }

    @Test
    public void getAllTags() throws DAOException {

        tagDAO.getAllTagsOfNews(4L);

        List<Tag> tags =  tagDAO.getAllTagsOfNews(4L);

        assertPropertyLenientEquals("tagName", "tag1", tags.get(0));
    }
}