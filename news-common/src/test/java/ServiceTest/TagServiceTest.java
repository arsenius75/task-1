package ServiceTest;

/**
 * Created by Arseni on 7/22/2015.
 */

import com.epam.newsmanagment.model.Tag;
import com.epam.newsmanagment.dao.ITagDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.ITagService;
import com.epam.newsmanagment.service.impl.TagService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

public class TagServiceTest {
    @Mock
    private ITagDAO tagDAO;
    private ITagService tagService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        tagService = new TagService();
        tagService.setTagDAO(tagDAO);
    }

    @Test
    public void getTag() throws DAOException, ServiceException {

        Tag mockedTag = new Tag();
        mockedTag.setTagName("tag");

        when(tagDAO.readItem(anyLong())).thenReturn(mockedTag);

        Tag tag = tagService.getTag(anyLong());

        assertEquals("tag", tag.getTagName());
        verify(tagDAO).readItem(anyLong());
    }

    @Test
    public void createTag() throws DAOException, ServiceException {

        Tag mockedTag = new Tag();
        mockedTag.setTagName("tag");

        when(tagDAO.addItem(mockedTag)).thenReturn(anyLong());

        Long id = tagService.createTag(mockedTag);

        assertEquals(Long.class, id.getClass());
        verify(tagDAO).addItem(mockedTag);
    }

    @Test
    public void updateTag() throws DAOException {

        Tag mockedTag = new Tag();
        mockedTag.setTagName("tag");

        DAOException e = new DAOException();
        doThrow(e).when(tagDAO).editItem(mockedTag);

        boolean isPassed = false;

        try {
            tagService.updateTag(mockedTag);
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(tagDAO).editItem(mockedTag);
    }

    @Test
    public void deleteTag() throws DAOException {

        DAOException e = new DAOException();
        doThrow(e).when(tagDAO).deleteItem(anyLong());

        boolean isPassed = false;

        try {
            tagService.deleteTag(anyLong());
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(tagDAO).deleteItem(anyLong());
    }


    @Test
    public void linkTag() throws DAOException {

        DAOException e = new DAOException();
        doThrow(e).when(tagDAO).link(anyLong(), anyLong());

        boolean isPassed = false;

        try {
            tagService.linkTagToNews(anyLong(), anyLong());
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(tagDAO).link(anyLong(), anyLong());

    }

    @Test
    public void unlinkTag() throws DAOException {


        DAOException e = new DAOException();
        doThrow(e).when(tagDAO).unlink(anyLong(), anyLong());

        boolean isPassed = false;

        try {
            tagService.unlinkTagToNews(anyLong(), anyLong());
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(tagDAO).unlink(anyLong(), anyLong());

    }

    @Test
    public void unlinkAllTags() throws DAOException {

        DAOException e = new DAOException();
        doThrow(e).when(tagDAO).unlinkAllTagsOfNews(anyLong());

        boolean isPassed = false;

        try {
            tagService.unlinkAllTagsFromNews(anyLong());
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(tagDAO).unlinkAllTagsOfNews(anyLong());

    }

    @Test
    public void getAllTags() throws DAOException, ServiceException {

        List<Tag> list = new ArrayList<Tag>();
        when(tagDAO.getAllTagsOfNews(anyLong())).thenReturn(list);

        List<Tag> resultList = tagService.getAllTagsOfNews(anyLong());

        assertEquals(resultList, list);
        verify(tagDAO).getAllTagsOfNews(anyLong());
    }
}