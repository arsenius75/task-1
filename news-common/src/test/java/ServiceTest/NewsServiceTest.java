package ServiceTest;


import com.epam.newsmanagment.model.News;
import com.epam.newsmanagment.model.SearchCriteria;
import com.epam.newsmanagment.dao.INewsDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.impl.NewsService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

public class NewsServiceTest {
    @Mock
    private INewsDAO newsDAO;
    private INewsService newsService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        newsService = new NewsService();
        newsService.setNewsDAO(newsDAO);
    }

    @Test
    public void getNews() throws DAOException, ServiceException {

        News mockedNews = new News();
        mockedNews.setTitle("title");
        mockedNews.setShortText("short text");
        mockedNews.setFullText("full text");

        when(newsDAO.readItem(anyLong())).thenReturn(mockedNews);

        News news = newsService.getNews(anyLong());


        assertEquals("title", news.getTitle());
        assertEquals("short text", news.getShortText());
        assertEquals("full text", news.getFullText());
        verify(newsDAO).readItem(anyLong());
    }

    @Test
    public void createNews() throws DAOException, ServiceException {

        News mockedNews = new News();
        mockedNews.setTitle("title");
        mockedNews.setShortText("short text");
        mockedNews.setFullText("full text");

        when(newsDAO.addItem(mockedNews)).thenReturn(anyLong());

        Long id = newsService.createNews(mockedNews);

        assertEquals(Long.class, id.getClass());

        verify(newsDAO).addItem(mockedNews);
    }

    @Test
    public void updateNews() throws DAOException {

        News mockedNews = new News();
        mockedNews.setTitle("title");
        mockedNews.setShortText("short text");
        mockedNews.setFullText("full text");

        DAOException e = new DAOException();
        doThrow(e).when(newsDAO).editItem(mockedNews);

        boolean isPassed = false;

        try {
            newsService.updateNews(mockedNews);
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(newsDAO).editItem(mockedNews);

    }


    @Test
    public void deleteNews() throws DAOException {

        DAOException e = new DAOException();
        doThrow(e).when(newsDAO).deleteItem(anyLong());

        boolean isPassed = false;

        try {
            newsService.deleteNews(anyLong());
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(newsDAO).deleteItem(anyLong());
    }

    @Test
    public void getCommentsCountForNews() throws DAOException, ServiceException {

        when(newsDAO.countAllNews()).thenReturn(100L);

        Long count = newsService.countAllNews();

        assertEquals(Long.class, count.getClass());

        verify(newsDAO).countAllNews();
    }

    @Test
    public void searchNews() throws DAOException, ServiceException {

        List<News> list = new ArrayList<News>();
        SearchCriteria searchCriteria = new SearchCriteria();
        when(newsDAO.searchNews(searchCriteria)).thenReturn(list);

        List<News> resultList = newsService.searchNews(searchCriteria);

        assertEquals(resultList, list);
        verify(newsDAO).searchNews(searchCriteria);
    }
}