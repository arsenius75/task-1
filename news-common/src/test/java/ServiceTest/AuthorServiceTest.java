package ServiceTest;

/**
 * Created by Arseni on 7/22/2015.
 */

import com.epam.newsmanagment.model.Author;
import com.epam.newsmanagment.dao.IAuthorDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.IAuthorService;
import com.epam.newsmanagment.service.impl.AuthorService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

public class AuthorServiceTest
{
    @Mock
    private IAuthorDAO authorDAO;
    private IAuthorService authorService;

    @Before
    public void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);
        authorService = new AuthorService();
        authorService.setAuthorDAO(authorDAO);
    }

    @Test
    public void getAuthor() throws DAOException, ServiceException {

        Author mockedAuthor = new Author();
        mockedAuthor.setAuthorName("Ololoev");

        when( authorDAO.readItem(anyLong())).thenReturn(mockedAuthor);

        Author author = authorService.getAuthor(anyLong());

        assertEquals("Ololoev", author.getAuthorName());
        verify(authorDAO).readItem(anyLong());

    }

    @Test
    public void createAuthor() throws DAOException, ServiceException {

        Author mockedAuthor = new Author();
        mockedAuthor.setAuthorName("Ololoev");

        when(authorDAO.addItem(mockedAuthor)).thenReturn(anyLong());

        Long id = authorService.createAuthor(mockedAuthor);

        assertEquals(Long.class, id.getClass());
        verify(authorDAO).addItem(mockedAuthor);
    }

    @Test
    public void updateAuthor() throws DAOException {

        Author mockedAuthor = new Author();
        mockedAuthor.setAuthorName("Ololoev");

        DAOException e = new DAOException();
        doThrow(e).when(authorDAO).editItem(mockedAuthor);


        boolean isPassed = false;

        try {
            authorService.updateAuthor(mockedAuthor);
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(authorDAO).editItem(mockedAuthor);
    }

    @Test
    public void deleteAuthor() throws DAOException {

        DAOException e = new DAOException();
        doThrow(e).when(authorDAO).deleteItem(anyLong());

        boolean isPassed = false;

        try {
            authorService.deleteAuthor(anyLong());
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(authorDAO).deleteItem(anyLong());
    }

    @Test
    public void linkAuthor() throws DAOException {


        DAOException e = new DAOException();
        doThrow(e).when(authorDAO).link(anyLong(), anyLong());


        boolean isPassed = false;

        try {
            authorService.linkAuthorToNews(anyLong(), anyLong());
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(authorDAO).link(anyLong(), anyLong());

    }

    @Test
    public void unlinkAuthor() throws DAOException {


        DAOException e = new DAOException();
        doThrow(e).when(authorDAO).unlink(anyLong());


        boolean isPassed = false;

        try {
            authorService.unlinkAuthorFromNews(anyLong());
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(authorDAO).unlink(anyLong());

    }

    @Test
    public void getAuthorOfNews() throws DAOException, ServiceException {

        Author mockedAuthor = new Author();
        mockedAuthor.setAuthorId(1L);
        mockedAuthor.setAuthorName("Ololoev");

        when( authorDAO.getAuthorOfNews(anyLong())).thenReturn(mockedAuthor);

        Author author = authorService.getAuthorOfNews(anyLong());

        assertEquals("Ololoev", author.getAuthorName());
        verify(authorDAO).getAuthorOfNews(anyLong());
    }


    @Test
    public void getAllAuthors() throws DAOException, ServiceException {

        List<Author> list = new ArrayList<Author>();
        when( authorDAO.getAllAuthors()).thenReturn(list);

        List<Author> resultList = authorService.getAllAuthors();

        assertEquals(resultList, list);
        verify(authorDAO).getAllAuthors();
    }
}