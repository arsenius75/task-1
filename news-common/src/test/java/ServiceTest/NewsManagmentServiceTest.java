package ServiceTest;


import com.epam.newsmanagment.model.*;
import com.epam.newsmanagment.dao.IAuthorDAO;
import com.epam.newsmanagment.dao.ICommentDAO;
import com.epam.newsmanagment.dao.INewsDAO;
import com.epam.newsmanagment.dao.ITagDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.*;
import com.epam.newsmanagment.service.impl.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

public class NewsManagmentServiceTest {




    @Mock
    private IAuthorDAO authorDAO;
    @Mock
    private ICommentDAO commentDAO;
    @Mock
    private INewsDAO newsDAO;
    @Mock
    private ITagDAO tagDAO;

    private INewsManagementService newsManagementService;

    @Before
    public void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);

        IAuthorService authorService = new AuthorService();
        authorService.setAuthorDAO(authorDAO);

        ICommentService commentService = new CommentService();
        commentService.setCommentDAO(commentDAO);

        INewsService newsService = new NewsService();
        newsService.setNewsDAO(newsDAO);

        ITagService tagService = new TagService();
        tagService.setTagDAO(tagDAO);


        newsManagementService = new NewsManagementService();

        newsManagementService.setAuthorService(authorService);
        newsManagementService.setCommentService(commentService);
        newsManagementService.setNewsService(newsService);
        newsManagementService.setTagService(tagService);
    }

    @Test
    public void addNews() throws DAOException, ServiceException {

        when(newsDAO.addItem(any(News.class))).thenReturn(1L);

        NewsManagementVO vo = new NewsManagementVO();
        vo.setAuthorItem(new Author(1L, "author", new Timestamp(20L)));
        vo.setNewsItem(new News());
        List<Long> tagIdList = new ArrayList<Long>();
        tagIdList.add(1L);
        vo.setTagIdList(tagIdList);

        newsManagementService.addNews(vo);

        verify(newsDAO).addItem(any(News.class));
        verify(authorDAO).link(anyLong(), anyLong());
        verify(tagDAO).link(anyLong(), anyLong());
    }

    @Test
    public void editNews() throws DAOException, ServiceException {

        when(newsDAO.addItem(any(News.class))).thenReturn(1L);

        NewsManagementVO vo = new NewsManagementVO();

        vo.setAuthorItem(new Author(1L, "author", new Timestamp(20L)));

        vo.setNewsItem(new News(1L, "t", "d", "d"));
        List<Tag> tagList = new ArrayList<Tag>();
        tagList.add(new Tag());
        vo.setTagList(tagList);

        List<Long> tagIdList = new ArrayList<Long>();
        tagIdList.add(1L);
        vo.setTagIdList(tagIdList);
        newsManagementService.editNews(vo);

        verify(newsDAO).editItem(any(News.class));

        verify(authorDAO).unlink(anyLong());
        verify(authorDAO).link(anyLong(), anyLong());

        verify(tagDAO).unlinkAllTagsOfNews(anyLong());
        verify(tagDAO).link(anyLong(), anyLong());
    }

    @Test
    public void deleteNews() throws DAOException, ServiceException {

        newsManagementService.deleteNews(anyLong());

        verify(tagDAO).unlinkAllTagsOfNews(anyLong());
        verify(authorDAO).unlink(anyLong());
        verify(commentDAO).deleteAllCommentsOfNews(anyLong());
        verify(newsDAO).deleteItem(anyLong());
    }

    @Test
    public void getSingleNews() throws DAOException, ServiceException {

        Object vo = newsManagementService.getSingleNews(anyLong());

        verify(newsDAO).readItem(anyLong());
        verify(authorDAO).getAuthorOfNews(anyLong());
        verify(tagDAO).getAllTagsOfNews(anyLong());
        verify(commentDAO).getAllCommentsForNews(anyLong());

        assertEquals(vo.getClass(), NewsManagementVO.class);
    }

    @Test
    public void addComment() throws DAOException, ServiceException {

        Object vo = newsManagementService.addComment("commentText", 1L);

        verify(commentDAO).addItem(any(Comment.class));

        assertEquals(vo.getClass(), Long.class);
    }

    @Test
    public void searchNews() throws DAOException, ServiceException {

        List<News> allNews = new ArrayList<News>();

        allNews.add(new News(1L, "t", "d", "d"));
        allNews.add(new News(2L, "e", "d", "d"));
        allNews.add(new News(3L, "t", "d", "d"));
        allNews.add(new News(4L, "t", "d", "d"));

        when(newsDAO.searchNews(any(SearchCriteria.class))).thenReturn(allNews);

        Object vo = newsManagementService.searchNews(new SearchCriteria());

        verify(newsDAO).searchNews(any(SearchCriteria.class));

        verify(authorDAO, times(4)).getAuthorOfNews(anyLong());
        verify(tagDAO, times(4)).getAllTagsOfNews(anyLong());
        verify(commentDAO, times(4)).getAllCommentsForNews(anyLong());

        assertEquals(vo.getClass(), ArrayList.class);
    }
}