package ServiceTest;


import com.epam.newsmanagment.model.Comment;
import com.epam.newsmanagment.dao.ICommentDAO;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.ICommentService;
import com.epam.newsmanagment.service.impl.CommentService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

public class CommentServiceTest {
    @Mock
    private ICommentDAO commentDAO;
    private ICommentService commentService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        commentService = new CommentService();
        commentService.setCommentDAO(commentDAO);
    }

    @Test
    public void getComment() throws DAOException, ServiceException {

        Comment mockedComment = new Comment();
        mockedComment.setCommentText("comment text");

        when(commentDAO.readItem(anyLong())).thenReturn(mockedComment);

        Comment comment = commentService.getComment(anyLong());

        assertEquals("comment text", comment.getCommentText());
        verify(commentDAO).readItem(anyLong());
    }

    @Test
    public void createComment() throws DAOException, ServiceException {

        Comment mockedComment = new Comment();
        mockedComment.setCommentText("comment text");

        when(commentDAO.addItem(mockedComment)).thenReturn(anyLong());

        Long id = commentService.createComment(mockedComment);

        assertEquals(Long.class, id.getClass());

        verify(commentDAO).addItem(mockedComment);

    }

    @Test
    public void updateComment() throws DAOException {

        Comment mockedComment = new Comment();
        mockedComment.setCommentText("comment text");

        DAOException e = new DAOException();
        doThrow(e).when(commentDAO).editItem(mockedComment);


        boolean isPassed = false;

        try {
            commentService.updateComment(mockedComment);
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(commentDAO).editItem(mockedComment);

    }


    @Test
    public void deleteAuthor() throws DAOException {

        DAOException e = new DAOException();
        doThrow(e).when(commentDAO).deleteItem(anyLong());

        boolean isPassed = false;

        try {
            commentService.deleteComment(anyLong());
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(commentDAO).deleteItem(anyLong());
    }

    @Test
    public void getAllCommentsForNews() throws DAOException, ServiceException {

        List<Comment> list = new ArrayList<Comment>();
        when(commentDAO.getAllCommentsForNews(anyLong())).thenReturn(list);

        List<Comment> resultList = commentService.getAllCommentsForNews(anyLong());

        assertEquals(resultList, list);
        verify(commentDAO).getAllCommentsForNews(anyLong());
    }

    @Test
    public void getCommentsCountForNews() throws DAOException, ServiceException {

        when(commentDAO.getCommentsCountForNews(anyLong())).thenReturn(100L);

        Long count = commentService.getCommentsCountForNews(anyLong());

        assertEquals(Long.class, count.getClass());

        verify(commentDAO).getCommentsCountForNews(anyLong());

    }

    @Test
    public void deleteAllCommentsOfNews() throws DAOException {

        DAOException e = new DAOException();
        doThrow(e).when(commentDAO).deleteAllCommentsOfNews(anyLong());

        boolean isPassed = false;

        try {
            commentService.deleteAllCommentsOfNews(anyLong());
        } catch (ServiceException e1) {
            isPassed = true;
        }
        assertTrue(isPassed);
        verify(commentDAO).deleteAllCommentsOfNews(anyLong());
    }
}