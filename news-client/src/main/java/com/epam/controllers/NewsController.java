package com.epam.controllers;

import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.NewsManagementVO;
import com.epam.newsmanagment.model.SearchCriteria;
import com.epam.newsmanagment.service.INewsManagementService;
import com.epam.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.util.List;

@Controller
@RequestMapping(value = "/newslist")
@SessionAttributes(types = SearchCriteria.class)
public class NewsController {

    private static int newsCountOnPage = 3;


    @Autowired
    private INewsManagementService newsManagementService;

    @ModelAttribute("searchCriteria")
    public SearchCriteria getSearchCriteriaObject() {

        SearchCriteria sc =  new SearchCriteria();
        return sc;
    }

    @RequestMapping(value = "/")
    public String showStartNewsPage(Model model) {

        try {
            model.addAttribute("authors", newsManagementService.getAllAuthors());
            model.addAttribute("tags", newsManagementService.getAllTags());

        } catch (ServiceException e) {
            return "error";
        }

        return "news";
    }

    @RequestMapping(value = "/search/page/{pageNumber}")
    public String searchNewsFromForm(@PathVariable Long pageNumber, @ModelAttribute("searchCriteria") SearchCriteria searchCriteria, Model model, BindingResult result) {

        List<NewsManagementVO> newsList = null;

        if(null == searchCriteria){
            searchCriteria = new SearchCriteria();
        }
        searchCriteria.setMinCount((pageNumber - 1) * newsCountOnPage + 1L);
        searchCriteria.setMaxCount((pageNumber) * newsCountOnPage);

        try {
            newsList = newsManagementService.searchNews(searchCriteria);

            model.addAttribute("authors", newsManagementService.getAllAuthors());
            model.addAttribute("tags", newsManagementService.getAllTags());
            model.addAttribute("pageCount", Util.getLongPageCount(newsManagementService.countAllNews(searchCriteria), newsCountOnPage));

        } catch (ServiceException e) {
            return "error";
        }

        model.addAttribute("newsList", newsList);
        model.addAttribute("currentPage", pageNumber);

        return "news";
    }
}

