package com.epam.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/")
public class StartPageController {

    @RequestMapping(value = "/")
    public String goToStart() {

        return "redirect:/newslist/search/page/1";
    }
}
