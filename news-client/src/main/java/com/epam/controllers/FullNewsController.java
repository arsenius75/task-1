package com.epam.controllers;

import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.model.*;
import com.epam.newsmanagment.service.INewsManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/newslist/full")
@SessionAttributes(types = SearchCriteria.class)
public class FullNewsController {


    @ModelAttribute("valueObject")
    public NewsManagementVO getValueObject() {
        NewsManagementVO newsManagementVO = new NewsManagementVO();
        newsManagementVO.setNewsItem(new News());
        newsManagementVO.setTagIdList(new ArrayList<Long>());
        newsManagementVO.setAuthorItem(new Author());

        return newsManagementVO;
    }

    @Autowired
    private INewsManagementService newsManagementService;

    @ModelAttribute("comment")
    public Comment getCommentObject() {
        return new Comment();
    }

    @RequestMapping(value = "/{newsId}")
    public String showFullNews(@PathVariable Long newsId, @ModelAttribute("searchCriteria") SearchCriteria searchCriteria, Model model, BindingResult result) {

        try {

            NewsManagementVO vo =  newsManagementService.getSingleNews(newsId);
            vo.getNewsItem().setRownum(newsManagementService.getRownumByNewsId(searchCriteria, newsId));

            model.addAttribute("news", vo);
            model.addAttribute("newsCount", newsManagementService.countAllNews(searchCriteria));

        } catch (ServiceException e) {
            return "error";
        }

        return "fullNews";
    }

    @RequestMapping(value = "/{newsId}/comment")
    public String postCommentToNews( @ModelAttribute("comment") @Valid  Comment comment, BindingResult result, RedirectAttributes redirectAttributes, Model model,
                                     @PathVariable Long newsId, @ModelAttribute("searchCriteria") SearchCriteria searchCriteria) {

        if (result.hasErrors()) {

            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.comment", result);
            redirectAttributes.addFlashAttribute("comment", comment);
            return "redirect:/newslist/full/" + newsId;
        }

        try {
            newsManagementService.addComment(comment.getCommentText(), newsId);
        } catch (ServiceException e) {
            return "error";
        }
        return "redirect:/newslist/full/" + newsId;
    }

    @RequestMapping(value = "/{newsId}/next")
    public String showNextNews(@PathVariable Long newsId, @ModelAttribute("searchCriteria") SearchCriteria searchCriteria, Model model, BindingResult result) {

        Long rownum = null;
        try {
            rownum = newsManagementService.getRownumByNewsId(searchCriteria, newsId);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        searchCriteria.setMinCount(rownum+1);
        searchCriteria.setMaxCount(rownum+1);

        List<NewsManagementVO> newsList = null;
        try {
            newsList = newsManagementService.searchNews(searchCriteria);

        } catch (ServiceException e) {
            return "error";
        }

        if (!newsList.isEmpty()) {
            return "redirect:/newslist/full/" + newsList.get(0).getNewsItem().getNewsId();
        }
        return "error";
    }

    @RequestMapping(value = "/{newsId}/previous")
    public String showPreviousNews(@PathVariable Long newsId, @ModelAttribute("searchCriteria") SearchCriteria searchCriteria, Model model, BindingResult result) {

        Long rownum = null;
        try {
            rownum = newsManagementService.getRownumByNewsId(searchCriteria, newsId);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        searchCriteria.setMinCount(rownum-1);
        searchCriteria.setMaxCount(rownum-1);


        //TODO remove to separate method
        List<NewsManagementVO> newsList = null;
        try {
            newsList = newsManagementService.searchNews(searchCriteria);
        } catch (ServiceException e) {
            return "error";
        }

        if (!newsList.isEmpty()) {
            return "redirect:/newslist/full/" + newsList.get(0).getNewsItem().getNewsId();
        }
        return "error";
    }

}
