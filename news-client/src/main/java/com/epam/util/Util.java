package com.epam.util;

import java.sql.Timestamp;
import java.util.Calendar;

public class Util {
    public static Timestamp getCurrentTimestamp(){
        Calendar calendar = Calendar.getInstance();
        java.util.Date now = calendar.getTime();
        return new Timestamp(now.getTime());
    }

    public static Long getLongPageCount(Long newsCount, int newsCountOnPage){
        Double doubleNewsCount = Double.valueOf(newsCount);
        Double pageCount = doubleNewsCount / newsCountOnPage;
        Long longPageCount = pageCount.longValue();

        if (pageCount > longPageCount) {
            longPageCount++;
        }
        return longPageCount;
    }
}
