
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<style>
    .error {
        color: #ff0000;
        font-style: italic;
        font-weight: bold;
    }
</style>

<br/>
<br/>
<br/>
      <div align="left">
          <spring:url value="/newslist/search/page/1" var="back" htmlEscape="true"/>
          <sf:form action="${back}">
              <button type="submit" class="btn btn-lg btn-primary">
                  <spring:message code="label.back" />
              </button>
          </sf:form>
      </div>

       <div style="padding: 10px;">
          <div style="display: inline">
                <span>
                    <b>
                        <c:out value="${news.newsItem.title}" />
                    </b>&nbsp;(
                    <spring:message code="label.news.author" />
                        <c:out value="${news.authorItem.authorName}" />)
                        <spring:message code="label.date_pattern" var="pattern"/>
                        <fmt:formatDate value="${news.newsItem.creationDate}" var="dateString" pattern="${pattern}" />
                </span>
                <span style="text-decoration: underline;">
                    <c:out value="${dateString}" />
                </span>
          </div>

          <div>
                <span style="white-space: pre">
                    <c:out value="${news.newsItem.fullText}" />
                </span>
          </div>

           <br/>

          <div align="left">
                <span style="color: gray;">
                     <c:forEach items="${news.commentList}" var="comment">
                         <div>
                             <div style="display: inline">
                                 <spring:message code="label.date_pattern" var="pattern"/>
                                 <fmt:formatDate value="${comment.creationDate}" var="dateString" pattern="${pattern}" />
                                <span style="text-decoration: underline;">
                                    <c:out value="${dateString}" />
                                </span>
                                 <span>
                                    <b>
                                        <c:out value="${comment.commentText}" />
                                    </b>&nbsp;
                                </span>
                             </div>
                         </div>

                     </c:forEach>
                </span>&nbsp;
          </div>
      </div>


      <div align="left">
          <spring:url value="/newslist/full/${news.newsItem.newsId}/comment" var="addComment" htmlEscape="true"/>

          <sf:form action="${addComment}" modelAttribute="comment">
              <tr>
                  <td><spring:message code="label.news.comment" />:</td>
                  <td><sf:input path="commentText" /></td>
                  <td>
                      <button type="submit" class="btn btn-lg btn-primary">
                          <spring:message code="label.send_comment" />
                      </button>
                  </td>
                  <td><sf:errors path="commentText" cssClass="error" /></td>
              </tr>
          </sf:form>
      </div>

      <c:if test="${news.newsItem.rownum!=1}">
          <div align="left">

              <spring:url value="/newslist/full/${news.newsItem.newsId}/previous" var="previous" htmlEscape="true"/>

              <a href="${previous}">
                      <spring:message code="label.previous" />
              </a>
          </div>
      </c:if>

      <c:if test="${news.newsItem.rownum < newsCount}">
          <div align="right">
              <spring:url value="/newslist/full/${news.newsItem.newsId}/next" var="next" htmlEscape="true"/>

              <a href="${next}">
                      <spring:message code="label.next" />
              </a>
          </div>
      </c:if>