<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
        <div >
            <div align="center" id="filter">
                <spring:url value="/newslist/search/page/1" var="search" htmlEscape="true"/>

                <sf:form method="post" action="${search}" modelAttribute="searchCriteria">
                    <sf:select path="authorId" id="select1">
                        <spring:message code="label.any_author" var="any_author" />
                        <sf:option value=""> <c:out value="${any_author}"/></sf:option>
                        <c:forEach var="author" items="${authors}">
                            <sf:option value="${author.authorId}">
                                <c:out value="${author.authorName}"/>
                            </sf:option>
                        </c:forEach>
                    </sf:select>

                    <span id="tags" style="text-align: left">
                            <sf:select multiple="true" tabindex="4" path="tagIDs" id="select2">
                                <c:forEach var="tag" items="${tags}">
                                    <sf:option value="${tag.tagId}">
                                        <c:out value="${tag.tagName}"/>
                                    </sf:option>
                                </c:forEach>
                            </sf:select>

                        <spring:message code="label.any_tag" var="any_tag" />
                        <spring:message code="label.any_author" var="any_author" />
                            <script>
                                $("#select2").SumoSelect({
                                    placeholder: '${any_tag}'
                                });
                                $("#select1").SumoSelect({
                                    placeholder: '${any_author}'
                                });
                            </script>
		            </span>


                    <button type="submit" name="action" value="filter" class="btn btn-lg btn-primary">
                        <spring:message code="label.filter" />
                    </button>
                </sf:form>

                <spring:url value="/newslist/" var="reset" htmlEscape="true"/>

                <sf:form method="post" action="${reset}" >

                    <button type="submit" name="action" value="reset" class="btn btn-lg btn-primary">
                        <spring:message code="label.reset" />
                    </button>
                </sf:form>
            </div>

        </div>
            <c:if test="${newsList.size()==0}">
                <div align="center">
                    <spring:message code="label.nothing_found" />
                </div>
            </c:if>




                <c:forEach items="${newsList}" var="newsVO">
                <div style="padding: 10px;" align="center">
                    <div style="display: inline">
                        <span>
                            <b>
                                <c:out value="${newsVO.newsItem.title}" />
                            </b>&nbsp;(
                            <spring:message code="label.news.author" />
                                <c:out value="${newsVO.authorItem.authorName}" />)
                            <spring:message code="label.date_pattern" var="pattern"/>
                                <fmt:formatDate value="${newsVO.newsItem.creationDate}" var="dateString" pattern="${pattern}" />
                        </span>
                        <span style="text-decoration: underline;">
                            <c:out value="${dateString}" />
                        </span>
                    </div>

                    <div>
                        <span style="white-space: pre">
                            <c:out value="${newsVO.newsItem.shortText}" />
                        </span>
                    </div>

                    <div align="right">
                        <span style="color: gray;">
                            <c:forEach items="${newsVO.tagList}" var="tag" varStatus="status">
                                <c:out value="${tag.tagName}" />
                                <c:if test="${!status.last}">,&nbsp;</c:if>
                            </c:forEach>
                        </span>&nbsp;

                        <spring:message code="label.news.comments" /> &nbsp;(<c:out value="${newsVO.commentList.size()}" />)&nbsp;

                        <spring:url value="/newslist/full/${newsVO.newsItem.newsId}" var="view" htmlEscape="true"/>
                        <a href="${view}"><spring:message code="label.news.view" /></a>

                        <spring:url value="/news/${newsVO.newsItem.newsId}/edit" var="edit" htmlEscape="true"/>
                        <a href="${edit}"><spring:message code="label.news.edit" /></a>

                    </div>
                </div>
            </c:forEach>

        <c:if test="${newsList.size()!=0}">
            <div align="center">
                <c:forEach var="i" begin="1" end="${pageCount}">
                    <c:if test="${currentPage==i}">
                        ${i}
                    </c:if>
                    <c:if test="${currentPage!=i}">

                        <spring:url value="/newslist/search/page/${i}" var="page" htmlEscape="true"/>

                        <a href="${page}">${i}</a>
                    </c:if>
                </c:forEach>
            </div>
        </c:if>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />