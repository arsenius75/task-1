<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

  <style type="text/css">
    body {
      margin:0px;
      padding:0px;
      height:100%;
      overflow:scroll;
    }

    .page {
      min-height:100%;
      position:relative;
    }

    .content {
      padding:10px;
      padding-bottom:20px; /* Height of the footer element */
      overflow:hidden;
    }


  </style>

<div class="page">
  <tiles:insertAttribute name="header" />
  <div class="content">
    <tiles:insertAttribute name="body" />
  </div>
  <tiles:insertAttribute name="footer" />
</div>
