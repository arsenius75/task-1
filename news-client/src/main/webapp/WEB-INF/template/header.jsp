<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<script type="text/javascript" src="<c:url value="/resources/js/jquery-1.11.3.min.js"/>"></script>

<link rel="stylesheet"
      href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/base/jquery-ui.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>

<script src="<c:url value="/resources/js/sumoselect/jquery.sumoselect.min.js"/>"></script>
<link href="<c:url value="/resources/js/sumoselect/sumoselect.css"/>" rel="stylesheet"/>





<style>
  .brd {
    border: 4px solid black;
    padding: 10px;
  }
</style>


<div class="header">
<div class="container">

    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand"><s:message code="label.newsPortal"/></a>
                <a class="navbar-brand">      </a>
                <a class="navbar-brand"><spring:message code="label.hello" /></a>


            </div>

            <div id="navbar" class="navbar-collapse collapse">


                <ul class="nav navbar-nav navbar-right">
                    <li><a href="?lang=en">EN</a></li>
                    <li><a href="?lang=ru">RU</a></li>
                </ul>
            </div>
        </div>
    </nav>
    </div>
</div>