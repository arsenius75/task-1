<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<footer id="footer">
    <div class="container" role="contentinfo">
        <div class="row" align="center">

    <spring:message code="label.footer" />
</div>
        </div>
    </footer>